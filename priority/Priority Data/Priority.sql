
2015-04-22 11:45:27.252 Priority[15603:3639962] {
    ActivityTBL = "<null>";
    City = "<null>";
    HomeTown = "<null>";
    ID = 2;
    InsertDate = "<null>";
    InsertUser = "<null>";
    Latitude = "<null>";
    Longitude = "<null>";
    OfferCustomerTBL = "<null>";
    Phone = 0599123456;
    UpdateDate = "<null>";
    UpdateUser = "<null>";
    Verfied = 0;
}

/*
Choose template
Choose one of these templates to start creating your Offer:

Multi-buy
Choose from popular combinations such as Buy One Get One Free and 3 for 2. A great way to get your customers trying more products and services.

Percentage discount
The straight-up % discount. You choose how much to take off, and what’s included in the deal.

Cashback
Encourage customer loyalty by offering a cashback on purchases over a certain amount.

Free delivery
Great for customers wanting to send gifts, get in the groceries, or have a special treat delivered at home.

Try for free
Get customers to try something new by giving away a free gift, product or service with a purchase.

Money off
Customers spend more when you make it easier for them. Set them a target and give them money off.
*/


INSERT INTO "OfferTypeTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '1','Multi-buy','0','Choose from popular combinations such as Buy One Get One Free and 3 for 2. A great way to get your customers trying more products and services.','1' );
INSERT INTO "OfferTypeTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '2','Percentage discount','0','The straight-up % discount. You choose how much to take off, and what’s included in the deal.','1' );
INSERT INTO "OfferTypeTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '3','Cashback','0','Encourage customer loyalty by offering a cashback on purchases over a certain amount.','1' );
INSERT INTO "OfferTypeTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '4','Free delivery','0','Great for customers wanting to send gifts, get in the groceries, or have a special treat delivered at home.','1' );
INSERT INTO "OfferTypeTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '5','Try for free','0','Get customers to try something new by giving away a free gift, product or service with a purchase.','1' );
INSERT INTO "OfferTypeTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '6','Money off','0','Customers spend more when you make it easier for them. Set them a target and give them money off.','1' );


/*
Food & Drink
Meals,Beverages,Restaurants,Coffee Shops etc... 

hopping
Travel
Health & Beauty
Entertainment
Music
Sports
Tickets
*/

INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '1','Food & Drink','0','Meals,Beverages,Restaurants,Coffee Shops etc... ','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '2','Hopping','0','-','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '3','Travel','0','-','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '4','Health & Beauty','0','-','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '5','Entertainment','0','-','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '6','Music','0','-','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '7','Sports','0','-	','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '8','Tickets','0','-','1' );
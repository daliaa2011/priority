CREATE TABLE "PersonTBL" (
	"ID" INTEGER PRIMARY KEY AUTOINCREMENT, 
	"Name" VARCHAR DEFAULT NULL,
	"UserName" VARCHAR DEFAULT NULL, 
	"BirthDate" DATE DEFAULT NULL, 
	"Email" VARCHAR DEFAULT NULL, 
	"PhoneNumber" VARCHAR, 
	"isMale" BOOLEAN DEFAULT 0, 
	"RegistrationDate" DATE DEFAULT CURRENT_DATE);

CREATE TABLE "OfferCategoryTBL" (
	"ID" INTEGER PRIMARY KEY AUTOINCREMENT, 
	"Name" VARCHAR, 
	"TypeID" INTEGER, 
	"Details " VARCHAR, 
	"isActive" BOOLEAN);

CREATE TABLE "OfferTypeTBL" (
	"ID" INTEGER PRIMARY KEY AUTOINCREMENT, 
	"Name" VARCHAR, 
	"TypeID" INTEGER, 
	"Details " VARCHAR, 
	"isActive" BOOLEAN);

CREATE TABLE "OfferTBL" (
	"ID" INTEGER PRIMARY KEY AUTOINCREMENT, 
	"Name" VARCHAR, 
	"BussinessesTBLID" INTEGER, 
	"Logo" VARCHAR, 
	"CatID" INTEGER, 
	"OfferTypesTBLID" INTEGER, 
	"Quantity" INTEGER, 
	"StartDate" DATE, 
	"EndDate" DATE, 
	"Phone" VARCHAR, 
	"Details" VARCHAR, 
	"OfferNum" INTEGER, 
	"GetFree" INTEGER, 
	"RestrictKeys" VARCHAR, 
	"RestrictType" VARCHAR, 
	"Sat" BOOLEAN, 
	"Sun" BOOLEAN, 
	"Mon" BOOLEAN, 
	"Tue" BOOLEAN, 
	"Wed" BOOLEAN, 
	"Thr" BOOLEAN, 
	"Fri" BOOLEAN, 
	"StartTime" TIME, 
	"EndTime" TIME, 
	"Cond1" BOOLEAN,
	"Cond2" BOOLEAN,
	"Cond3" BOOLEAN,
	"Cond4" BOOLEAN,
	"Cond5" BOOLEAN, 
	"Cond6" BOOLEAN, 
	"Cond7" BOOLEAN, 
	"ExtraCond1" BOOLEAN, 
	"ExtraCond2" BOOLEAN,
	"ExtraCond3" BOOLEAN,
	"VCode" VARCHAR,
	"IsStaticV" BOOLEAN, 
	"CodeType" INTEGER, 
	"CodeQuote" VARCHAR, 
	"InsertDate" DATETIME,
	"UpdateDate" DATETIME,
	"isActive" BOOLEAN);


INSERT INTO "OfferTypeTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '1','Multi-buy','0','Choose from popular combinations such as Buy One Get One Free and 3 for 2. A great way to get your customers trying more products and services.','1' );
INSERT INTO "OfferTypeTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '2','Percentage discount','0','The straight-up % discount. You choose how much to take off, and what’s included in the deal.','1' );
INSERT INTO "OfferTypeTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '3','Cashback','0','Encourage customer loyalty by offering a cashback on purchases over a certain amount.','1' );
INSERT INTO "OfferTypeTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '4','Free delivery','0','Great for customers wanting to send gifts, get in the groceries, or have a special treat delivered at home.','1' );
INSERT INTO "OfferTypeTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '5','Try for free','0','Get customers to try something new by giving away a free gift, product or service with a purchase.','1' );
INSERT INTO "OfferTypeTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '6','Money off','0','Customers spend more when you make it easier for them. Set them a target and give them money off.','1' );

INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '1','Food & Drink','0','Meals,Beverages,Restaurants,Coffee Shops etc... ','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '2','Hopping','0','-','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '3','Travel','0','-','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '4','Health & Beauty','0','-','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '5','Entertainment','0','-','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '6','Music','0','-','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '7','Sports','0','-	','1' );
INSERT INTO "OfferCategoryTBL" ( "ID","Name","TypeID","Details ","isActive" ) VALUES ( '8','Tickets','0','-','1' );
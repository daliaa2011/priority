//
//  SideMenuModel.m
//  Priority
//
//  Created by AnAs EiD on 4/27/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "SideMenuModel.h"

@implementation SideMenuModel

- (SharedDataObject *)theAppDataObject{
    id<AppDataDelegate>delegate = (id<AppDataDelegate>)[UIApplication sharedApplication].delegate;
    SharedDataObject *dataObject;
    dataObject = (SharedDataObject *)delegate.theSharedDataObject;
    return dataObject;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        sharedDataObject = [self theAppDataObject];
        if ([sharedDataObject _DBObject] == nil) {
            [sharedDataObject set_DBObject:[[PriorityDAO alloc] init]];
        }
    }
    return self;
}
- (NSMutableArray *)getOfferCategories {
    NSMutableArray *offerCategories = [[NSMutableArray alloc]init];
    [[sharedDataObject _DBObject]connectDB];
    offerCategories = [[sharedDataObject _DBObject]getOfferCategories:"SELECT * FROM OfferCategoryTBL WHERE isActive = 1"];
    [[sharedDataObject _DBObject]disConnectDB];
    return offerCategories;
}

-(BOOL) isCategoryExist:(OfferCategoryList*) catgoryObj {
    NSMutableArray *offerCategories = [[NSMutableArray alloc]init];
    [[sharedDataObject _DBObject]connectDB];
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM OfferCategoryTBL WHERE isActive = 1 AND ID = %d",catgoryObj._ID];
    offerCategories = [[sharedDataObject _DBObject]getOfferCategories:[query UTF8String]];
    [[sharedDataObject _DBObject]disConnectDB];
    return (offerCategories.count > 0);
}
-(void) insertOrUpdateCategory:(OfferCategoryList*) catgoryObj {
    if ([self isCategoryExist:catgoryObj]) {
        // update
    }
    else {
        [[sharedDataObject _DBObject]connectDB];
        // insert
        NSString *query = [[NSString alloc]initWithFormat:@"INSERT INTO OfferCategoryTBL (ID,Name,TypeID,Details,isActive,IconName,NameEn) VALUES ('%d','%@',%d,'%@','%d','%@','%@')",
                 catgoryObj._ID,
                 catgoryObj.name,
                 catgoryObj.typeID,
                 catgoryObj.details,
                 catgoryObj.isActive,
                 catgoryObj.iconName,
                 catgoryObj.NameEn
                 ];
        [[sharedDataObject _DBObject]insertData:query];
        [[sharedDataObject _DBObject]disConnectDB];

    }
}
@end

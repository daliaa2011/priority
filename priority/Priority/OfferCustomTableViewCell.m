//
//  OfferCustomTableViewCell.m
//  Priority
//
//  Created by AnAs EiD on 4/29/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "OfferCustomTableViewCell.h"

@implementation OfferCustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.logoImageView.alpha = 0.85;
    self.logoImageView.layer.masksToBounds = true;
    self.logoImageView.layer.cornerRadius = self.logoImageView.frame.size.width/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

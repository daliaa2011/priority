//
//  OfferCustomTableViewCell.h
//  Priority
//
//  Created by AnAs EiD on 4/29/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "AppDataDelegate.h"
#import "SharedDataObject.h"

@interface OfferCustomTableViewCell : UITableViewCell{
    
}


@property (weak, nonatomic) IBOutlet UIImageView *offerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *offerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *businessNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *offerDetailsLabel;
@end

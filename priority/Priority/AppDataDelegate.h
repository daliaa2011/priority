//
//  AppDataDelegate.h
//  Priority
//
//  Created by AnAs EiD on 4/28/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AppDataObject;

@protocol AppDataDelegate <NSObject>

- (AppDataObject *)theSharedDataObject;


@end

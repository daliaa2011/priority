//
//  SharedDataObject.m
//  Priority
//
//  Created by AnAs EiD on 4/12/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "SharedDataObject.h"
#import "ServerAPI.h"
#import "OfferCategoryList.h"
#import "SideMenuModel.h"

@implementation SharedDataObject

-(void) loadCategories {
    [ServerAPI getOffersCategoriesSuccess:^(id response) {
        if ([response isKindOfClass:[NSArray class]]) {
            SideMenuModel *model = [[SideMenuModel alloc] init];

            for (NSMutableArray *categoryDic in response) {
                OfferCategoryList *obj = [[OfferCategoryList alloc]init];;
                obj._ID = [[categoryDic valueForKey:@"ID"] intValue];
                obj.name = [categoryDic valueForKey:@"Name"];
                obj.NameEn = [categoryDic valueForKey:@"NameEn"];
                obj.details = [categoryDic valueForKey:@"Details"];
                obj.isActive = [[categoryDic valueForKey:@"IsActive"] intValue];
                [model insertOrUpdateCategory:obj];
            }
        }
    } failure:^(NSString *errorString) {
        
    }];
}

@end


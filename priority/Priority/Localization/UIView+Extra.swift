//
//  UIView+Extra.swift
//  Canary
//
//  Created by Esraa Apady on 1/27/17.
//  Copyright © 2017 canary. All rights reserved.
//

import UIKit

public extension UIView {
    
    
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    public func localizeSubViews() {
        self.subviews.forEach { (view) in
            if let tf = view as? UITextField {
                if !tf.localizationKey.isEmpty {
                    tf.localize = true
                }
            }else if let lbl = view as? UILabel {
                if !lbl.localizationKey.isEmpty {
                    lbl.localize = true
                }
            }else if let btn = view as? UIButton {
                if !btn.localizationKey.isEmpty {
                    btn.localize = true
                }
            }else {
                view.localizeSubViews()
            }
            
        }
    }

}

@IBDesignable extension UIView {
    @IBInspectable var borderColor: UIColor? {
        set {
            layer.borderColor = newValue?.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            } else {
                return nil
            }
        }
    }
}

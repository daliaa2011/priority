//
//  LocalizeExtensions.swift
//  CanaryUser
//
//  Created by Esraa Apady on 3/3/17.
//  Copyright © 2017 Dalia. All rights reserved.
//

import UIKit

@IBDesignable extension UILabel {
    
    @IBInspectable var localize: Bool {
        set (value){
            if value {
                self.text = NSLocalizedString(localizationKey, comment: "")
            }
        }
        get {
            return false
        }
    }
    
    @IBInspectable var localizationKey: String {
        set (value){
            self.localizationKeyString = value
        }
        get {
            guard let text = self.localizationKeyString else { return "" }
            return text
        }
    }
    
}

@IBDesignable extension UIButton {
    
    @IBInspectable var localize: Bool {
        set (value){
            if value {
                self.setTitle(NSLocalizedString(localizationKey, comment: ""), for: .normal)

            }
        }
        get {
            return false
        }
    }
    
    @IBInspectable var localizationKey: String {
        set (value) {
            self.localizationKeyString = value
        }
        get {
            guard let text = self.localizationKeyString else { return "" }
            return text
        }
    }
    
}

private var localizationString: String = ""

extension UILabel {
    
    var localizationKeyString: String? {
        get {
            return objc_getAssociatedObject(self, &localizationString) as? String
        }
        set (newValue) {
            objc_setAssociatedObject(self, &localizationString, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func setLineHeight(_ lineHeight: CGFloat) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.0
        paragraphStyle.lineHeightMultiple = lineHeight
        paragraphStyle.alignment = self.textAlignment
        
        guard let text = self.text else {
            return
        }
        let attrString = NSMutableAttributedString(string: text)
        attrString.addAttribute(NSFontAttributeName, value: self.font, range: NSRange(location: 0, length: attrString.length))
        attrString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSRange(location: 0, length: attrString.length))
        self.attributedText = attrString
    }
}

extension UIButton {
    
    var localizationKeyString: String? {
        get {
            return objc_getAssociatedObject(self, &localizationString) as? String
        }
        set (newValue) {
            objc_setAssociatedObject(self, &localizationString, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}


@IBDesignable extension UITextField {
    
    @IBInspectable var localize: Bool {
        set (value){
            if value {
                let attributes = [NSForegroundColorAttributeName: self.textColor ?? UIColor.black, NSFontAttributeName: self.font ?? UIFont.systemFont(ofSize: 16.0)]
                self.attributedPlaceholder = NSAttributedString(string: NSLocalizedString(localizationKey, comment: ""),attributes: attributes)
 
            }
        }
        get {
            return false
        }
    }
    
    @IBInspectable var localizationKey: String {
        set (value) {
            self.localizationKeyString = value
        }
        get {
            guard let text = self.localizationKeyString else { return "" }
            return text
        }
    }
    
}

extension UITextField {
    
    var localizationKeyString: String? {
        get {
            return objc_getAssociatedObject(self, &localizationString) as? String
        }
        set (newValue) {
            objc_setAssociatedObject(self, &localizationString, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}


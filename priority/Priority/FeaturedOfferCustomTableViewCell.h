//
//  FeaturedOfferCustomTableViewCell.h
//  Priority
//
//  Created by AnAs EiD on 4/27/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "SharedDataObject.h"
#import "AppDataObject.h"

@interface FeaturedOfferCustomTableViewCell : UITableViewCell{
    
}

@property (strong, nonatomic) IBOutlet UIImageView *offerImageImageView;
@property (strong, nonatomic) IBOutlet UIImageView * offerLogoImageView;
@property (strong, nonatomic) IBOutlet UILabel *offerTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *offerCategoryLabel;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
//@property (strong, nonatomic) IBOutlet UIImageView *distanceIconImageView;
//@property (strong, nonatomic) IBOutlet UIImageView *categoryIconImageView;

@end

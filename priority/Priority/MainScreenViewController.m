//
//  MainScreenViewController.m
//  Priority
//
//  Created by AnAs EiD on 4/13/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "MainScreenViewController.h"
#import "MFSideMenu.h"
#import "TermsViewController.h"
#import "FAQViewController.h"

@interface MainScreenViewController (){
    NSMutableArray *featuredOffers;
    NSMutableArray *featuredOffersSorted;
}
@end

@implementation MainScreenViewController

- (SharedDataObject *)theAppDataObject{
    id<AppDataDelegate>delegate = (id<AppDataDelegate>)[UIApplication sharedApplication].delegate;
    SharedDataObject *dataObject;
    dataObject = (SharedDataObject *)delegate.theSharedDataObject;
    return dataObject;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        sharedDataObject = [self theAppDataObject];
        mainScreenModel = [[MainScreenModel alloc]init];
        [sharedDataObject set_DBObject:[[PriorityDAO alloc] init]];
        [self getCurrentLocation];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [[self menuContainerViewController]setPanMode:MFSideMenuPanModeDefault];
    
    //transparent NavBar.
    //[[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"navBar"] forBarMetrics:UIBarMetricsDefault];
    // [[[self navigationController]navigationBar]setShadowImage:[UIImage new]];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[[self navigationController]navigationBar]setTranslucent:NO];
    
    [[[self navigationController]navigationBar]setBarTintColor:[UIColor colorWithRed:235.0/255.0 green:86.0/255.0 blue:62.0/255.0 alpha:1]];
    
    [self.featuredOffersTableView registerNib:[UINib nibWithNibName:@"FeaturedOfferCustomTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"featuredOfferCell"];
    
    self.featuredOffersTableView.backgroundColor = [UIColor clearColor];
//    //=======UI Stuff======
//    if ([_featuredOffersTableView respondsToSelector:@selector(setLayoutMargins:)]) {
//        [_featuredOffersTableView setLayoutMargins:UIEdgeInsetsZero];
//    }
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    titleLabel.text = NSLocalizedString(@"priority", @"priority");
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi" size:16.0];
    titleLabel.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabel;
    
    
    UIButton *rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 20, 20)];
    [rightButton setImage:[UIImage imageNamed:@"Menu"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    [[self navigationItem]setRightBarButtonItem:rightBarButton];
    
    UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 20, 20)];
    [leftButton setImage:[UIImage imageNamed:@"Search"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(searchOffers) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    [[self navigationItem]setLeftBarButtonItem:leftBarButton];
    
    /*
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Search"] style:UIBarButtonItemStylePlain target:self action:@selector(rightSideMenuButtonPressed)];
    [leftBarButton setTintColor:[UIColor whiteColor]];
    [[self navigationItem]setLeftBarButtonItem:leftBarButton];
    //=======

    //ActivationViewController *activationView = [[ActivationViewController alloc]init];
    //[self presentViewController:activationView animated:YES completion:^{}];
    
    NSDictionary *params = @{@"Phone": @"0599123456"};
    
    [ServerAPI sendCustomerInfoWithParams:params
                                  Success:^(id response) {
                                      NSLog(@"%@",response);
                                  }
                                  failure:^(NSString *errorString) {
                                      NSLog(@"%@",errorString);
                                  }];
    */
    
    [sharedDataObject setMainHUD:[MBProgressHUD showHUDAddedTo:[self view] animated:YES]];
    [[sharedDataObject mainHUD]setMode:MBProgressHUDModeIndeterminate];
    [[sharedDataObject mainHUD]setDetailsLabelText:@"جاري التحميل"];
    [[sharedDataObject mainHUD]setDimBackground:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 * Get the location for the device at App Launch
 *
 */
- (void)getCurrentLocation {
    [sharedDataObject setLocationManager:[[CLLocationManager alloc]init]];
    [[sharedDataObject locationManager] setDelegate:self];
    [[sharedDataObject locationManager] setDesiredAccuracy:kCLLocationAccuracyBest];
    [[sharedDataObject locationManager] setDistanceFilter:kCLLocationAccuracyBestForNavigation];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [[sharedDataObject locationManager] requestWhenInUseAuthorization];
    [[sharedDataObject locationManager] startUpdatingLocation];
}

/**
 * Set distance in offers objects and sort them by nearest
 *
 */
- (void)sortOffersArray {
    featuredOffersSorted = [NSMutableArray array];
    for (int i = 0; i < [featuredOffers count]; i++) {
        [[featuredOffers objectAtIndex:i]setObject:[NSNumber numberWithInt:[[sharedDataObject currentLocation] distanceFromLocation:[[featuredOffers objectAtIndex:i]objectForKey:@"businessLocation"]]] forKey:@"businessDistance"];
    }
    [featuredOffers sortUsingDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"businessDistance" ascending:YES], nil]];
    NSLog(@"DONE");
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    //get & Store current location in sharedDataObject
    [sharedDataObject setCurrentLocation:[sharedDataObject locationManager].location];
    [[sharedDataObject locationManager] stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            NSLog(@"User still thinking..");
        } break;
        case kCLAuthorizationStatusDenied: {
            NSLog(@"User hates you");
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"خطأ" message:@"قم بتفعيل الموقع الجغرافي من الإعدادات" delegate:nil cancelButtonTitle:@"إخفاء" otherButtonTitles:nil, nil];
            [alert show];
            [[sharedDataObject mainHUD]setHidden:YES];
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            //Fetching offers is permitted after getting location privileges
            [ServerAPI getFeaturedOffersWithSuccess:^(id response) {
                featuredOffers = [NSMutableArray array];
                for (NSMutableArray *rawOffer in response) {
                    //OfferObject *offer = [[OfferObject alloc]initWithResponce:rawOffer];
                    [featuredOffers addObject:[[OfferObject alloc] createArrayObjectWithResponce:rawOffer]];
                }
                [self sortOffersArray];
                [[sharedDataObject mainHUD]setHidden:YES];
                [_featuredOffersTableView reloadData];
            } Failure:^(NSString *errorString) {
                NSLog(@"Error : %@",errorString);
            }];
            //[locationManager startUpdatingLocation]; //Will update location immediately
        } break;
        default:
            break;
    }
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FeaturedOfferCustomTableViewCell *featuredOfferCell = [tableView dequeueReusableCellWithIdentifier:@"featuredOfferCell"];
    featuredOfferCell.backgroundColor = [UIColor clearColor];
    featuredOfferCell.contentView.backgroundColor = [UIColor clearColor];
    featuredOfferCell.offerLogoImageView.layer.cornerRadius = featuredOfferCell.offerLogoImageView.frame.size.width/2;
    featuredOfferCell.offerLogoImageView.layer.masksToBounds  = YES;
    
    //get offer logo
    if ([[featuredOffers objectAtIndex:[indexPath row]]objectForKey:@"offerLogo"]) {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = [featuredOfferCell offerImageImageView];
        [[featuredOfferCell offerImageImageView]sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/offers/%@",[[featuredOffers objectAtIndex:indexPath.row]objectForKey:@"offerLogo"]]]
                                                             andPlaceholderImage:[UIImage imageNamed:@"placeholders"]
                                                                         options:SDWebImageProgressiveDownload
                                                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                if (!activityIndicator) {
                                                                                    [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                                    [activityIndicator setCenter:[weakImageView center]];
                                                                                    [activityIndicator startAnimating];
                                                                                }
                                                                            });
                                                                            
                                                                        }
                                                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [activityIndicator removeFromSuperview];
                                                                               activityIndicator = nil;
                                                                           });
                                                                           
                                                                        }];
    }
    //get business logo
    if ([[featuredOffers objectAtIndex:[indexPath row]]objectForKey:@"businessLogo"]) {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = [featuredOfferCell offerLogoImageView];
        [[featuredOfferCell offerLogoImageView]sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/company/%@",[[featuredOffers objectAtIndex:indexPath.row]objectForKey:@"businessLogo"]]]
                                                             andPlaceholderImage:nil
                                                                         options:SDWebImageProgressiveDownload
                                                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                if (!activityIndicator) {
                                                                                    [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                                    [activityIndicator setCenter:[weakImageView center]];
                                                                                    [activityIndicator startAnimating];
                                                                                }
                                                                            });
                                                                            
                                                                        }
                                                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [activityIndicator removeFromSuperview];
                                                                               activityIndicator = nil;
                                                                               //NSLog(@"error : %@",error);
                                                                           });
                                                                           
                                                                       }];
    }
    //other offer info
    [[featuredOfferCell offerTitleLabel]setText:[[featuredOffers objectAtIndex:[indexPath row]]objectForKey:@"Title"]];
    
    [[featuredOfferCell offerCategoryLabel]setText:[mainScreenModel getCategoryNameForID:[[[featuredOffers objectAtIndex:[indexPath row]]objectForKey:@"CatsTBLID"] intValue]]];
    [[featuredOfferCell distanceLabel]setText:[NSString stringWithFormat:@"%0.2f كم",[[[featuredOffers objectAtIndex:indexPath.row]objectForKey:@"businessDistance"] floatValue]/1000.0]];
//    [[featuredOfferCell categoryIconImageView]setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%ld",(long)[[[featuredOffers objectAtIndex:[indexPath row]]objectForKey:@"CatsTBLID"] intValue]]]];
    return featuredOfferCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [featuredOffers count];
}

#pragma mark - UITabelViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    OfferDetailsViewController *OfferDetailsView = [[OfferDetailsViewController alloc]initWithNibName:@"OfferDetailsViewController" bundle:nil OfferID:[[[featuredOffers objectAtIndex:[indexPath row]]objectForKey:@"offerID"] intValue]];
    [[self navigationController]pushViewController:OfferDetailsView animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat screenwidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = ((screenwidth - 20) * (166.0/300)) + 60 + 15;
    return height;
}

#pragma mark - UserDefiendFunctions
- (void)rightSideMenuButtonPressed {
    [sharedDataObject.sideMenu toggleMenu];
}

- (void)searchOffers {
    SearchViewController *searchView = [[SearchViewController alloc]initWithNibName:@"SearchViewController" bundle:nil];
    [[self navigationController]pushViewController:searchView animated:YES];
}

- (void) goToHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    [sharedDataObject.sideMenu toggleMenu];
}

- (void) goToMySection
{
    MyPriorityViewController *myPriorityView = [[MyPriorityViewController alloc]init];
    [[self navigationController]pushViewController:myPriorityView animated:YES];
    [sharedDataObject.sideMenu toggleMenu];
}
- (void) goToCategory:(NSInteger) categoryID
{
    CategoryOffersViewController *categoryOffersView = [[CategoryOffersViewController alloc]initWithNibName:@"CategoryOffersViewController" bundle:nil CategoryID:categoryID];
    [[self navigationController]pushViewController:categoryOffersView animated:NO];
    [sharedDataObject.sideMenu toggleMenu];
}

- (void) goToFAQ {
    FAQViewController *controller = [[FAQViewController alloc]init];
    [[self navigationController]pushViewController:controller animated:YES];
    [sharedDataObject.sideMenu toggleMenu];

}

- (void) goToTerms {
    TermsViewController *controller = [[TermsViewController alloc]init];
    [[self navigationController]pushViewController:controller animated:YES];
    [sharedDataObject.sideMenu toggleMenu];

}
@end





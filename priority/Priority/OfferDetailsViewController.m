//
//  OfferDetailsViewController.m
//  Priority
//
//  Created by AnAs EiD on 5/4/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "OfferDetailsViewController.h"
#import "MainScreenModel.h"
#import "btSimpleSideMenu.h"
#import "Priority-Swift.h"
#import "SharedManager.h"

@interface OfferDetailsViewController (){
    
    MainScreenModel* mainScreenModel;
    NSDateComponents *components;
    
    NSMutableDictionary *offer;
    int scrollHeight;
    BOOL used;
    BOOL saved;
    
    UIImageView *mainOfferImage;//Header Image For Offer
    UIImageView *logoOfferImage;//Corner Image For Offer
    UILabel *sep1;//Separator 1
    UILabel *offerTitleLabel; //Offer Title
    UILabel *businessNameLabel;// Business Name
    UILabel *sep2;//Separator 2
    UILabel *distanceLabel;// Distance between client and business Value
    UIImageView *distanceIcon;//Distance icon
    UILabel *sep3;//Separator 3
    UIButton *useOfferButton;//Use Button
    UIButton *saveOfferButton;// Save Button
    UILabel *sep4;//Separator 4
    UITextView *offerDetails;//Details
    UIButton *expandeDetailsButton;
    UILabel *sep41;//Separator 5
    UILabel *text1Label;//Text Label
    GMSMapView *mapView;
    UILabel *businessAdressLabel; //Business Address
    UILabel *sep5;//Separator 6
    UILabel *text2Label;//Text Label
    UITextView *termsConditionsLabel;// Terms Label
    UIButton *expandeTermsButton;
}
- (void)initScrollView:(id)response;
- (void)expandViewWithTag:(id)sender;
- (void)saveOffer;
- (void)useOfferOrShowCode;
@end

@implementation OfferDetailsViewController

- (SharedDataObject *)theAppDataObject{
    id<AppDataDelegate>delegate = (id<AppDataDelegate>)[UIApplication sharedApplication].delegate;
    SharedDataObject *dataObject;
    dataObject = (SharedDataObject *)delegate.theSharedDataObject;
    return dataObject;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil OfferID:(int)offerID{
    self = [super init];
    if (self) {
        sharedDataObject = [self theAppDataObject];
        if ([sharedDataObject _DBObject] == nil) {
            [sharedDataObject set_DBObject:[[PriorityDAO alloc] init]];
        }
        offerDetailsModel = [[OfferDetailsModel alloc]init];
        mainScreenModel = [[MainScreenModel alloc]init];
        ID = offerID;
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view localizeSubViews];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self menuContainerViewController]setPanMode:MFSideMenuPanModeNone];
    
    self.offerLogoImageView.layer.cornerRadius = self.offerLogoImageView.frame.size.width/2;
    self.offerLogoImageView.layer.masksToBounds  = YES;
    self.offerLogoImageView.alpha = 0.85;


    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    titleLabel.text = NSLocalizedString(@"priority", @"priority");
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi" size:16.0];
    titleLabel.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabel;
    
    
    UIButton *rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 30, 30)];
    [rightButton setImage:[UIImage imageNamed:@"Share"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(shareTapped) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    [[self navigationItem]setRightBarButtonItem:rightBarButton];
    
    UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 30, 30)];
    [leftButton setImage:[UIImage imageNamed:@"Back"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    [[self navigationItem]setLeftBarButtonItem:leftBarButton];
    
    
    self.useButton.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.useButton.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.useButton.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    
    self.saveButton.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.saveButton.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.saveButton.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    
    
    [sharedDataObject setMainHUD:[MBProgressHUD showHUDAddedTo:[self view] animated:YES]];
    [[sharedDataObject mainHUD]setMode:MBProgressHUDModeIndeterminate];
    [[sharedDataObject mainHUD]setDimBackground:YES];
    [[sharedDataObject mainHUD]setDetailsLabelText:@"جاري التحميل"];
    //Get Offer Details
    [ServerAPI getOfferWithID:ID
                      Success:^(id response) {
                          offer = [[OfferObject alloc] createArrayObjectWithResponce:response];
                          used = [offerDetailsModel checkIfOfferUsed:offer];
                          if (used) {
                              [offer setObject:[offerDetailsModel getOfferVCode:ID] forKey:@"offerVCode"];
                          }
                          saved = [offerDetailsModel checkIfOfferSaved:offer];
                        //  [self initScrollView:offer];
                          [self fillViewWithData:offer];
                          [[sharedDataObject mainHUD]hide:YES];
                      }
                      failure:^(NSString *errorString) {
                          NSLog(@"Error : %@",errorString);
                          [[sharedDataObject mainHUD]hide:YES];
                      }];
}

#pragma mark - initUI

-(void) shareTapped {
    NSString * title =[NSString stringWithFormat:@"%@\n%@",self.offerTitleLabel.text,self.offerDetailsLabel.text];
    NSArray* dataToShare = @[title];
    UIActivityViewController* activityViewController =[[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypeAirDrop];
    [self presentViewController:activityViewController animated:YES completion:^{}];
}

- (void) fillViewWithData:(id)response
{
    if (saved) {
        //remove.from.favourite
        [self.saveButton setTitle:NSLocalizedString(@"remove.from.favourite", @"remove.from.favourite") forState:UIControlStateNormal];
    }else {
        //add.to.favourite
        [self.saveButton setTitle:NSLocalizedString(@"add.to.favourite", @"add.to.favourite") forState:UIControlStateNormal];
    }
    
    if ([response objectForKey:@"offerLogo"]) {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = mainOfferImage;
        [self.offerImageImageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/offers/%@",[response objectForKey:@"offerLogo"]]]
                                              andPlaceholderImage:[UIImage imageNamed:@"placeholders"]
                                                          options:SDWebImageProgressiveDownload
                                                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                                 if (!activityIndicator) {
                                                                     [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                     [activityIndicator setCenter:[weakImageView center]];
                                                                     [activityIndicator startAnimating];
                                                                 }
                                                             });
                                                             
                                                         }
                                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [activityIndicator removeFromSuperview];
                                                                activityIndicator = nil;
                                                                //NSLog(@"error : %@",error);
                                                            });
                                                            
                                                        }];
    }
    
    if ([response objectForKey:@"businessLogo"]) {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = logoOfferImage;
        NSLog(@"%@",[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/offers/%@",[response objectForKey:@"businessLogo"]]);
        [self.offerLogoImageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/company/%@",[response objectForKey:@"businessLogo"]]]
                                              andPlaceholderImage:nil
                                                          options:SDWebImageProgressiveDownload
                                                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                                 if (!activityIndicator) {
                                                                     [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                     [activityIndicator setCenter:[weakImageView center]];
                                                                     [activityIndicator startAnimating];
                                                                 }
                                                             });
                                                             
                                                         }
                                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [activityIndicator removeFromSuperview];
                                                                activityIndicator = nil;
                                                                NSLog(@"error : %@",error);
                                                            });
                                                            
                                                        }];
    }
    
    [self.offerTitleLabel setText:[response objectForKey:@"Title"]];
    [self.offerCategoryLabel setText:[mainScreenModel getCategoryNameForID:[[response objectForKey:@"CatsTBLID"] intValue]]];
    [self.distanceLabel setText:[NSString stringWithFormat:@"%0.2f كم",[[response objectForKey:@"businessDistance"] floatValue]/1000.0]];
    
    
    
    if ([response objectForKey:@"OfferDesc"]) {
        [self.offerDetailsLabel setText:[response objectForKey:@"OfferDesc"]];
    }
   
    self.offerShortDesLabel.layer.borderWidth = 0.5;
    self.offerShortDesLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    switch ([[response objectForKey:@"offerCodeType"] intValue]) {
        case 1:
        {
            [self.offerShortDesLabel setText:[NSString stringWithFormat:@"اشتري %d %@ و احصل على %d",[[response objectForKey:@"offerNum"] intValue],[response objectForKey:@"offerName"],[[response objectForKey:@"offerGetFree"] intValue]]];
        }
            break;
        case 2:
        {
            [self.offerShortDesLabel setText:[NSString stringWithFormat:@"احصل على نسبة خصم %d %%",[[response objectForKey:@"offerGetFree"] intValue]]];
        }
            break;
        case 3:
        {
            [self.offerShortDesLabel setText:[NSString stringWithFormat:@"أنفق %d و استرد %d",[[response objectForKey:@"offerNum"] intValue],[[response objectForKey:@"offerGetFree"] intValue]]];
            
        }
            break;
        case 4:
        {
            [self.offerShortDesLabel setText:[NSString stringWithFormat:@"توصيل مجاني إلى %d ميل",[[response objectForKey:@"offerNum"] intValue]]];
        }
            break;
        case 5:
        {
            [self.offerShortDesLabel setText:[NSString stringWithFormat:@"جرب %@ مجانا",[response objectForKey:@"Title"]]];
        }
            break;
        case 6:
        {
            [self.offerShortDesLabel setText:[NSString stringWithFormat:@"خصم %d من %@",[[response objectForKey:@"offerNum"] intValue],[response objectForKey:@"offerName"]]];
        }
            break;
        default:
            break;
    }
    
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[(CLLocation *)[response objectForKey:@"businessLocation"] coordinate].latitude longitude:[(CLLocation *)[response objectForKey:@"businessLocation"] coordinate].longitude zoom:18];
    mapView = [GMSMapView mapWithFrame:self.mapContainerView.bounds
                                camera:camera];
    
    [mapView setMyLocationEnabled:YES];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = camera.target;
    marker.snippet = [response objectForKey:@"businessName"];
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = mapView;
    mapView.autoresizingMask =  UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.mapContainerView addSubview:mapView];
   
    if (![[response objectForKey:@"businessName"] isEqual:[NSNull null]]) {
         [self.businessNameLabel setText:[response objectForKey:@"businessName"]];
    }
    else {
        [self.businessNameLabel setText:@""];
    }
    
    NSString *address = @"";
    NSString *city = @"";
    if (![[response objectForKey:@"businessAddress"]isEqual:[NSNull null]]) {
        address = [[SharedManager sharedInstance] getStringValue:[response objectForKey:@"businessAddress"]];
    }
    if (![[response objectForKey:@"businessCity"]isEqual:[NSNull null]]) {
        city = [[SharedManager sharedInstance] getStringValue:[response objectForKey:@"businessCity"]];
    }
    
    [self.addressLabel setText:[NSString stringWithFormat:@"%@\n%@",address,city]];
    
    
    [self.saveButton addTarget:self action:@selector(saveOffer) forControlEvents:UIControlEventTouchUpInside];

    BOOL englishLang = [[NSUserDefaults standardUserDefaults] boolForKey:@"englishLang"];
    
    if (used){
        if (englishLang) {
            [self.useButton setTitle:@"Show offer code" forState:UIControlStateNormal];
        }
        else {
            [self.useButton setTitle:@"اعرض الرمز" forState:UIControlStateNormal];
        }
        
    }
    else{
        if (englishLang) {
            [self.useButton setTitle:@"Use it now" forState:UIControlStateNormal];
        }
        else {
            [self.useButton setTitle:@"استعمل العرض" forState:UIControlStateNormal];
        }
        
        
    }
    [self.useButton addTarget:self action:@selector(useOfferOrShowCode) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSString* conditionsString = @"";
    if ([[response objectForKey:@"Cond1"] boolValue])
        conditionsString = [conditionsString stringByAppendingString:@"هذا العرض لا يمكن استخدامه مع عرض اخر"];
    
    if ([[response objectForKey:@"Cond2"] boolValue])
    {
        if (conditionsString.length == 0)
            conditionsString = [conditionsString stringByAppendingString:@"لا يمكن استخدامه في الاجازات الرسمية"];
        else
            conditionsString = [conditionsString stringByAppendingString:@"\nلا يمكن استخدامه في الاجازات الرسمية"];
    }
    
    if ([[response objectForKey:@"Cond3"] boolValue])
    {
        if (conditionsString.length == 0)
            conditionsString = [conditionsString stringByAppendingString:@"كود العرض لكل عملية شراء"];
        else
            conditionsString = [conditionsString stringByAppendingString:@"\nكود العرض لكل عملية شراء"];
    }
    if ([[response objectForKey:@"Cond4"] boolValue])
    {
        if (conditionsString.length == 0)
            conditionsString = [conditionsString stringByAppendingString:@"كود العرض لكل زبون"];
        else
            conditionsString = [conditionsString stringByAppendingString:@"\nكود العرض لكل زبون"];
    }
    if ([[response objectForKey:@"Cond5"] boolValue])
    {
        if (conditionsString.length == 0)
            conditionsString = [conditionsString stringByAppendingString:@"الرجاء عرض كود العرض عند الشراء"];
        else
            conditionsString = [conditionsString stringByAppendingString:@"\nالرجاء عرض كود العرض عند الشراء"];
    }
    if ([[response objectForKey:@"Cond6"] boolValue])
    {
        if (conditionsString.length == 0)
            conditionsString = [conditionsString stringByAppendingString:@"الرجاء عرض الكود عند الطلب"];
        else
            conditionsString = [conditionsString stringByAppendingString:@"\nالرجاء عرض الكود عند الطلب"];
    }
    if ([[response objectForKey:@"Cond7"] boolValue])
    {
        if (conditionsString.length == 0)
            conditionsString = [conditionsString stringByAppendingString:@"هذا العرض لا يمكن استخدامه مع عرض اخر"];
        else
            conditionsString = [conditionsString stringByAppendingString:@"\nهذا العرض لا يمكن استخدامه مع عرض اخر"];
    }
    
    NSArray *conditionsArray = [response valueForKey:@"OfferConditionsTBLs"];
    if (![conditionsArray isEqual:[NSNull null]]&& [conditionsArray isKindOfClass:[NSArray class]]) {
        for (NSMutableArray *dic in conditionsArray) {
            NSString *condName = [dic valueForKey:@"Name"];
            if (![condName isEqual:[NSNull null]]&& [condName isKindOfClass:[NSString class]]) {
                conditionsString = [conditionsString stringByAppendingString:[NSString stringWithFormat:@"\u2022 %@\n",[condName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]];
            }
        }
    }
    
    
    self.conditionsLabel.text = conditionsString;
    
    BOOL sat = [[response objectForKey:@"Sat"] boolValue];
    BOOL sun = [[response objectForKey:@"Sun"] boolValue];
    BOOL mon = [[response objectForKey:@"Mon"] boolValue];
    BOOL tue = [[response objectForKey:@"Tue"] boolValue];
    BOOL wed = [[response objectForKey:@"Wed"] boolValue];
    BOOL thr = [[response objectForKey:@"Thr"] boolValue];
    BOOL fri = [[response objectForKey:@"Fri"] boolValue];
    if (sat || sun || mon || tue || wed || thr || fri)
    {
        NSString* restrictionString = @"";
        if (sat && sun && mon && tue && wed && thr && fri)
            restrictionString = [restrictionString stringByAppendingString:@"هذا العرض ساري جميع ايام الاسبوع \n"];
        else
        {
            restrictionString = @"هذا العرض ساري في ايام ";
            if (sat)
                restrictionString = [restrictionString stringByAppendingString:@"السبت "];
            if (sun)
                restrictionString = [restrictionString stringByAppendingString:@"الاحد "];
            if (mon)
                restrictionString = [restrictionString stringByAppendingString:@"الاثنين "];
            if (tue)
                restrictionString = [restrictionString stringByAppendingString:@"الثلاثاء "];
            if (wed)
                restrictionString = [restrictionString stringByAppendingString:@"الاربعاء "];
            if (thr)
                restrictionString = [restrictionString stringByAppendingString:@"الخميس "];
            if (fri)
                restrictionString = [restrictionString stringByAppendingString:@"الجمعة "];
        }
        
        
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"HH:mm"];
//        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
//        NSString *timestamp = [dateFormatter stringFromDate:[response objectForKey:@"offerEndDate"]];
        self.restrictionsLabel.text = restrictionString;
    }
    
    
}

- (void)initScrollView:(id)response{
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    CGRect frame;
    //======= Main Offer Image =======//
    mainOfferImage = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 0.0, SCREEN_WIDTH, 300.0)];
    //[mainOfferImage setImage:[UIImage imageNamed:@"mobile21"]];
    [mainOfferImage setContentMode:UIViewContentModeScaleAspectFill];
    if ([response objectForKey:@"offerLogo"]) {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = mainOfferImage;
        [mainOfferImage sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/offers/%@",[response objectForKey:@"offerLogo"]]]
                                              andPlaceholderImage:[UIImage imageNamed:@"placeholders"]
                                                          options:SDWebImageProgressiveDownload
                                                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                                 if (!activityIndicator) {
                                                                     [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                     [activityIndicator setCenter:[weakImageView center]];
                                                                     [activityIndicator startAnimating];
                                                                 }
                                                             });
                                                             
                                                         }
                                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [activityIndicator removeFromSuperview];
                                                                activityIndicator = nil;
                                                                //NSLog(@"error : %@",error);
                                                            });
                                                            
                                                        }];
    }
    [_mainScrollView addSubview:mainOfferImage];
    scrollHeight = mainOfferImage.frame.size.height;
    //======= Logo Offer Image =======//
    logoOfferImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 60.0, 240.0, 60.0, 60.0)];
    if ([response objectForKey:@"businessLogo"]) {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = logoOfferImage;
        NSLog(@"%@",[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/offers/%@",[response objectForKey:@"businessLogo"]]);
        [logoOfferImage sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/company/%@",[response objectForKey:@"businessLogo"]]]
                                                                     andPlaceholderImage:nil
                                                                                 options:SDWebImageProgressiveDownload
                                                                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                                        if (!activityIndicator) {
                                                                                            [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                                            [activityIndicator setCenter:[weakImageView center]];
                                                                                            [activityIndicator startAnimating];
                                                                                        }
                                                                                    });
                                                                                    
                                                                                }
                                                                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                                       [activityIndicator removeFromSuperview];
                                                                                       activityIndicator = nil;
                                                                                       NSLog(@"error : %@",error);
                                                                                   });
                                                                                   
                                                                               }];
    }
    
    [_mainScrollView addSubview:logoOfferImage];
    scrollHeight += logoOfferImage.frame.size.height;
    //======= Separator 1 =======//
    sep1 = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 301.0, SCREEN_WIDTH, 1.0)];
    [sep1 setBackgroundColor:[UIColor colorWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1]];
    [_mainScrollView addSubview:sep1];
    scrollHeight += 1;
    //======= Offer Title Label =======//
    offerTitleLabel = [[UILabel alloc]init];
    [offerTitleLabel setText:[response objectForKey:@"offerName"]];
    [offerTitleLabel setNumberOfLines:0];
    [offerTitleLabel setFont:[UIFont fontWithName:@"Droid Arabic Kufi" size:18.0]];
    [offerTitleLabel setTextAlignment:NSTextAlignmentRight];
    //[offerTitleLabel setBackgroundColor:[UIColor lightGrayColor]];
    
    frame = [[offerTitleLabel text]boundingRectWithSize:CGSizeMake(SCREEN_WIDTH, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{ NSFontAttributeName: [UIFont fontWithName:@"Droid Arabic Kufi" size:18.0],NSParagraphStyleAttributeName: paragraphStyle} context:nil];
    
    [offerTitleLabel setFrame:CGRectMake(0.0,302.0,SCREEN_WIDTH - 5,frame.size.height)];
    
    [_mainScrollView addSubview:offerTitleLabel];
    scrollHeight += offerTitleLabel.frame.size.height;
    //======= BusinessNameLabel =======//
    businessNameLabel = [[UILabel alloc]init];
    [businessNameLabel setText:[response objectForKey:@"businessName"]];
    [businessNameLabel setFont:[UIFont fontWithName:@"Droid Arabic Kufi" size:16.0]];
    [businessNameLabel setTextAlignment:NSTextAlignmentRight];
    //[businessNameLabel setBackgroundColor:[UIColor blueColor]];
    frame = [[businessNameLabel text]boundingRectWithSize:CGSizeMake(SCREEN_WIDTH, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Droid Arabic Kufi" size:16.0],NSParagraphStyleAttributeName : paragraphStyle} context:nil];
    [businessNameLabel setFrame:CGRectMake(0.0,
                                     offerTitleLabel.frame.origin.y + offerTitleLabel.frame.size.height,
                                     SCREEN_WIDTH - 5,
                                     frame.size.height)];
    [_mainScrollView addSubview:businessNameLabel];
    scrollHeight += businessNameLabel.frame.size.height;
    //======= Separator 2 =======//
    sep2 = [[UILabel alloc]initWithFrame:CGRectMake(0.0,
                                                    businessNameLabel.frame.origin.y + businessNameLabel.frame.size.height + 1,
                                                    SCREEN_WIDTH,
                                                    1.0)];
    [sep2 setBackgroundColor:[UIColor colorWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1]];
    [_mainScrollView addSubview:sep2];
    scrollHeight += 1;
    
    
    [offer setObject:[NSNumber numberWithInt:[[sharedDataObject currentLocation] distanceFromLocation:[response objectForKey:@"businessLocation"]]] forKey:@"businessDistance"];
    //======= Distance Label =======//
    distanceLabel = [[UILabel alloc]init];
    [distanceLabel setText:[NSString stringWithFormat:@"%0.2f كم",[[response objectForKey:@"businessDistance"] floatValue]/1000.0]];
    [distanceLabel setFont:[UIFont fontWithName:@"Droid Arabic Kufi" size:14.0]];
    [distanceLabel setTextAlignment:NSTextAlignmentRight];
    [distanceLabel setTextColor:[UIColor colorWithRed:34/255.0 green:179.0/255.0 blue:232.0/255.0 alpha:1]];
    frame = [[distanceLabel text]boundingRectWithSize:CGSizeMake(SCREEN_WIDTH, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Droid Arabic Kufi" size:14.0],NSParagraphStyleAttributeName : paragraphStyle} context:nil];
    [distanceLabel setFrame:CGRectMake(0.0,
                                       sep2.frame.origin.y + sep2.frame.size.height,
                                       SCREEN_WIDTH - 25,
                                       frame.size.height)];
    [_mainScrollView addSubview:distanceLabel];
    //======= Distance Icon =======//
    distanceIcon = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 23,
                                                               sep2.frame.origin.y + sep2.frame.size.height,
                                                                23.0, 23.0)];
    [distanceIcon setImage:[UIImage imageNamed:@"nav"]];
    [_mainScrollView addSubview:distanceIcon];
    
    scrollHeight += distanceLabel.frame.size.height;
    //======= Separator 3 =======//
    sep3 = [[UILabel alloc]initWithFrame:CGRectMake(0.0,
                                                    distanceLabel.frame.origin.y + distanceLabel.frame.size.height + 1,
                                                    SCREEN_WIDTH,
                                                    1.0)];
    [sep3 setBackgroundColor:[UIColor colorWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1]];
    [_mainScrollView addSubview:sep3];
    scrollHeight += 1;
    //======= Use Offer Button =======//
    useOfferButton = [[UIButton alloc]init];
    [[useOfferButton titleLabel]setFont:[UIFont fontWithName:@"Droid Arabic Kufi" size:14.0]];
    [useOfferButton setFrame:CGRectMake(0.4 * SCREEN_WIDTH,
                                        sep3.frame.origin.y + sep3.frame.size.height + 10.0,
                                        0.5 * SCREEN_WIDTH,
                                        50.0)];
    [useOfferButton setBackgroundColor:[UIColor blackColor]];
    [[useOfferButton layer]setCornerRadius:25.0];
    [useOfferButton setClipsToBounds:YES];
    [_mainScrollView addSubview:useOfferButton];
    //======= Save Offer Button =======//
    saveOfferButton = [[UIButton alloc]init];
    [saveOfferButton setImage:[UIImage imageNamed:@"save"] forState:UIControlStateNormal];
    [saveOfferButton setFrame:CGRectMake(0.1 * SCREEN_WIDTH,
                                        sep3.frame.origin.y + sep3.frame.size.height + 10.0,
                                        0.25 * SCREEN_WIDTH,
                                        50.0)];
    if (saved) {
        [saveOfferButton setBackgroundColor:[UIColor grayColor]];
    }
    else{
        [saveOfferButton setBackgroundColor:[UIColor blackColor]];
    }
    [[saveOfferButton layer]setCornerRadius:25.0];
    [saveOfferButton setClipsToBounds:YES];
    [saveOfferButton addTarget:self action:@selector(saveOffer) forControlEvents:UIControlEventTouchUpInside];
    [_mainScrollView addSubview:saveOfferButton];
    
    if (used){
        [useOfferButton setTitle:@"اعرض الرمز" forState:UIControlStateNormal];
        [saveOfferButton setEnabled:NO];
    }
    else{
        [useOfferButton setTitle:@"استعمل العرض" forState:UIControlStateNormal];
        [saveOfferButton setEnabled:YES];
    }
    [useOfferButton addTarget:self action:@selector(useOfferOrShowCode) forControlEvents:UIControlEventTouchUpInside];
    //======= Separator 4 =======//
    sep4 = [[UILabel alloc]initWithFrame:CGRectMake(0.0,
                                                    useOfferButton.frame.origin.y + useOfferButton.frame.size.height + 11,
                                                    SCREEN_WIDTH,
                                                    1.0)];
    [sep4 setBackgroundColor:[UIColor colorWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1]];
    [_mainScrollView addSubview:sep4];
    
    scrollHeight += 50.0;
    //======= expande Details Button =======//
    expandeDetailsButton = [[UIButton alloc]initWithFrame:CGRectMake(5.0, sep4.frame.origin.y + 130, 25.0, 25.0)];
    [expandeDetailsButton setTitle:@"^" forState:UIControlStateNormal];
    [expandeDetailsButton addTarget:self action:@selector(expandViewWithTag:) forControlEvents:UIControlEventTouchUpInside];
    [expandeDetailsButton setTag:10];
    //[expandeDetailsButton setBackgroundColor:[UIColor greenColor]];
    [expandeDetailsButton setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
    [_mainScrollView addSubview:expandeDetailsButton];
    //======= Offer Details =======//
    offerDetails = [[UITextView alloc]init];
    if (![response objectForKey:@"offerDetails"]) {
        [offerDetails setText:[response objectForKey:@"offerDetails"]];
    }
    else {
        //[offerDetails setText:@"لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق ليتراسيت (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل ألدوس بايج مايكر (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم."];
        
        switch ([[response objectForKey:@"offerCodeType"] intValue]) {
            case 1:
            {
                [offerDetails setText:[NSString stringWithFormat:@"اشتري %d %@ و احصل على %d",[[response objectForKey:@"offerNum"] intValue],[response objectForKey:@"offerName"],[[response objectForKey:@"offerGetFree"] intValue]]];
            }
                break;
            case 2:
            {
                [offerDetails setText:[NSString stringWithFormat:@"احصل على نسبة خصم %d %%",[[response objectForKey:@"offerGetFree"] intValue]]];
            }
                break;
            case 3:
            {
                [offerDetails setText:[NSString stringWithFormat:@"أنفق %d و استرد %d",[[response objectForKey:@"offerNum"] intValue],[[response objectForKey:@"offerGetFree"] intValue]]];
                
            }
                break;
            case 4:
            {
                [offerDetails setText:[NSString stringWithFormat:@"توصيل مجاني إلى %d ميل",[[response objectForKey:@"offerNum"] intValue]]];
            }
                break;
            case 5:
            {
                [offerDetails setText:[NSString stringWithFormat:@"جرب %@ مجانا",[response objectForKey:@"offerName"]]];
            }
                break;
            case 6:
            {
                [offerDetails setText:[NSString stringWithFormat:@"خصم %d من %@",[[response objectForKey:@"offerNum"] intValue],[response objectForKey:@"offerName"]]];
            }
                break;
            default:
                break;
        }
    }
    [offerDetails setFont:[UIFont fontWithName:@"Droid Arabic Kufi" size:14.0]];
    [offerDetails setTextAlignment:NSTextAlignmentRight];
    //[offerDetails setBackgroundColor:[UIColor darkGrayColor]];
    [offerDetails setScrollEnabled:NO];
    [offerDetails setEditable:NO];
    
    [offerDetails setFrame:CGRectMake(30.0,
                                      sep4.frame.origin.y + sep4.frame.size.height,
                                      SCREEN_WIDTH - 30.0,
                                      150.0)];
    [_mainScrollView addSubview:offerDetails];
    scrollHeight += offerDetails.frame.size.height;
    //======= Separator 5 =======//
    sep41 = [[UILabel alloc]initWithFrame:CGRectMake(0.0,
                                                    offerDetails.frame.origin.y + offerDetails.frame.size.height + 4,
                                                    SCREEN_WIDTH,
                                                    1.0)];
    [sep41 setBackgroundColor:[UIColor colorWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1]];
    [_mainScrollView addSubview:sep41];
    //======= Text label =======//
    text1Label = [[UILabel alloc]init];
    [text1Label setText:@"العنوان"];
    [text1Label setFont:[UIFont fontWithName:@"Droid Arabic Kufi" size:16.0]];
    [text1Label setTextAlignment:NSTextAlignmentRight];
    [text1Label setFrame:CGRectMake(0.0, offerDetails.frame.origin.y + offerDetails.frame.size.height, SCREEN_WIDTH, 30.0)];
    [_mainScrollView addSubview:text1Label];
    scrollHeight += text1Label.frame.size.height;
    //======= Map View =======//
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[(CLLocation *)[response objectForKey:@"businessLocation"] coordinate].latitude longitude:[(CLLocation *)[response objectForKey:@"businessLocation"] coordinate].longitude zoom:18];
    mapView = [GMSMapView mapWithFrame:CGRectMake(0.0,
                                                  text1Label.frame.origin.y + text1Label.frame.size.height,
                                                  SCREEN_WIDTH,
                                                  180.0)
                                camera:camera];

    [mapView setMyLocationEnabled:YES];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = camera.target;
    marker.snippet = [response objectForKey:@"businessName"];
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = mapView;
    
    [_mainScrollView addSubview:mapView];
    scrollHeight += 180.0;
    //======= Bussiness Adress Label =======//
    businessAdressLabel = [[UILabel alloc]init];
    [businessAdressLabel setText:[NSString stringWithFormat:@"%@\n%@",[response objectForKey:@"businessAddress"],[response objectForKey:@"businessCity"]]];
    [businessAdressLabel setNumberOfLines:0];
    [businessAdressLabel setFont:[UIFont fontWithName:@"Droid Arabic Kufi" size:14.0]];
    [businessAdressLabel setTextAlignment:NSTextAlignmentRight];
    
    frame = [[businessAdressLabel text]boundingRectWithSize:CGSizeMake(SCREEN_WIDTH, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Droid Arabic Kufi" size:14.0],NSParagraphStyleAttributeName : paragraphStyle} context:nil];
    [businessAdressLabel setFrame:CGRectMake(0.0,
                                             mapView.frame.origin.y + mapView.frame.size.height,
                                             SCREEN_WIDTH,
                                             frame.size.height)];
    [_mainScrollView addSubview:businessAdressLabel];
    scrollHeight += businessAdressLabel.frame.size.height;
    //======= Separator 6 =======//
    sep5 = [[UILabel alloc]initWithFrame:CGRectMake(0.0,
                                                    businessAdressLabel.frame.origin.y + businessAdressLabel.frame.size.height + 11,
                                                    SCREEN_WIDTH,
                                                    1.0)];
    [sep5 setBackgroundColor:[UIColor colorWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1]];
    [_mainScrollView addSubview:sep5];
    scrollHeight += 1;
    //======= Expand Button =======//
    expandeTermsButton = [[UIButton alloc]initWithFrame:CGRectMake(5.0, sep5.frame.origin.y + 10, 25.0, 25.0)];
    [expandeTermsButton setTitle:@"^" forState:UIControlStateNormal];
    [expandeTermsButton addTarget:self action:@selector(expandViewWithTag:) forControlEvents:UIControlEventTouchUpInside];
    [expandeTermsButton setTag:12];
    //[expandeTermsButton setBackgroundColor:[UIColor greenColor]];
    [expandeTermsButton setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
    [_mainScrollView addSubview:expandeTermsButton];
    //======= Text Label =======//
    text2Label = [[UILabel alloc]init];
    [text2Label setText:@"الشروط والأحكام"];
    [text2Label setFont:[UIFont fontWithName:@"Droid Arabic Kufi" size:16.0]];
    [text2Label setTextAlignment:NSTextAlignmentRight];
    [text2Label setFrame:CGRectMake(0.0, sep5.frame.origin.y + sep5.frame.size.height, SCREEN_WIDTH, 40.0)];
    [_mainScrollView addSubview:text2Label];
    scrollHeight += text2Label.frame.size.height;
    //======= Terms label =======//
    termsConditionsLabel = [[UITextView alloc]init];
    [termsConditionsLabel setText:@"لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق ليتراسيت (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل ألدوس بايج مايكر (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم."];
    [termsConditionsLabel setFont:[UIFont fontWithName:@"Droid Arabic Kufi" size:14.0]];
    [termsConditionsLabel setTextAlignment:NSTextAlignmentRight];
    //[termsConditionsLabel setBackgroundColor:[UIColor darkGrayColor]];
    [termsConditionsLabel setScrollEnabled:NO];
    [termsConditionsLabel setEditable:NO];
    
    [termsConditionsLabel setFrame:CGRectMake(30.0,
                                      text2Label.frame.origin.y + text2Label.frame.size.height,
                                      SCREEN_WIDTH - 30.0,
                                      0)];
    [_mainScrollView addSubview:termsConditionsLabel];
    scrollHeight += termsConditionsLabel.frame.size.height;
    
    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, scrollHeight)];
}

- (void)expandViewWithTag:(id)sender {
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    CGRect frame;
    if ([sender tag] == 10 || [sender tag] == 11) {
        frame = [[offerDetails text]boundingRectWithSize:CGSizeMake(SCREEN_WIDTH, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Droid Arabic Kufi" size:14.0],NSParagraphStyleAttributeName : paragraphStyle} context:nil];
        
        [UIView animateWithDuration:0.5 animations:^{
            if ([sender tag] == 10) {
                [offerDetails setFrame:CGRectMake(30.0,
                                                  sep4.frame.origin.y + sep4.frame.size.height,
                                                  SCREEN_WIDTH - 30,
                                                  frame.size.height + 50.0)];
                [expandeDetailsButton setImage:[UIImage imageNamed:@"less"] forState:UIControlStateNormal];
                [expandeTermsButton setImage:[UIImage imageNamed:@"less"] forState:UIControlStateNormal];
            }
            else if ([sender tag] == 11){
                scrollHeight = scrollHeight - ceil(offerDetails.frame.size.height) + 150.0;
                [offerDetails setFrame:CGRectMake(30.0,
                                                  sep4.frame.origin.y + sep4.frame.size.height,
                                                  SCREEN_WIDTH - 30,
                                                  150.0)];
                [expandeDetailsButton setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
                [expandeTermsButton setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
            }
            [expandeDetailsButton setFrame:CGRectMake(5.0, offerDetails.frame.origin.y + offerDetails.frame.size.height - 25, 25.0, 25.0)];
            
            [sep41 setFrame:CGRectMake(0.0,
                                      expandeDetailsButton.frame.origin.y + expandeDetailsButton.frame.size.height,
                                      SCREEN_WIDTH,
                                      1.0)];
            [text1Label setFrame:CGRectMake(0.0,
                                            offerDetails.frame.origin.y + offerDetails.frame.size.height,
                                            SCREEN_WIDTH,
                                            30.0)];
            [mapView setFrame:CGRectMake(0.0,
                                         text1Label.frame.origin.y + text1Label.frame.size.height,
                                         SCREEN_WIDTH,
                                         180.0)];
            [businessAdressLabel setFrame:CGRectMake(0.0,
                                                     mapView.frame.origin.y + mapView.frame.size.height,
                                                     SCREEN_WIDTH,
                                                     businessAdressLabel.frame.size.height)];
            [sep5 setFrame:CGRectMake(0.0,
                                      businessAdressLabel.frame.origin.y + businessAdressLabel.frame.size.height + 11,
                                      SCREEN_WIDTH,
                                      1.0)];
            [expandeTermsButton setFrame:CGRectMake(5.0, sep5.frame.origin.y + 10, 25.0, 25.0)];
            
            [text2Label setFrame:CGRectMake(0.0, sep5.frame.origin.y + sep5.frame.size.height, SCREEN_WIDTH, 40.0)];
            
            [termsConditionsLabel setFrame:CGRectMake(30.0,
                                                      text2Label.frame.origin.y + text2Label.frame.size.height,
                                                      SCREEN_WIDTH - 30.0,
                                                      0)];
            
        } completion:^(BOOL finished) {
            if ([sender tag] == 10) {
                scrollHeight += ceil(offerDetails.frame.size.height) - 150.0;
                [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, scrollHeight)];
                [sender setTag:11];
            }
            else if ([sender tag] == 11){
                [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, scrollHeight)];
                [sender setTag:10];
            }
        }];
    }
    else if ([sender tag] == 12 || [sender tag] == 13){
        frame = [[termsConditionsLabel text]boundingRectWithSize:CGSizeMake(SCREEN_WIDTH, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Droid Arabic Kufi" size:14.0],NSParagraphStyleAttributeName : paragraphStyle} context:nil];
        [UIView animateWithDuration:0.5 animations:^{
            if ([sender tag] == 12) {
                [termsConditionsLabel setFrame:CGRectMake(30.0,
                                                  text2Label.frame.origin.y + text2Label.frame.size.height,
                                                  SCREEN_WIDTH - 30,
                                                  frame.size.height + 50.0)];
                
                
            }
            else if ([sender tag] == 13){
                scrollHeight -= ceil(termsConditionsLabel.frame.size.height);
                [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, scrollHeight)];
                
            }
        } completion:^(BOOL finished) {
            if ([sender tag] == 12) {
                scrollHeight += ceil(termsConditionsLabel.frame.size.height);
                [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, scrollHeight)];
                [sender setTag:13];
            }
            else if ([sender tag] == 13){
                [termsConditionsLabel setFrame:CGRectMake(30.0,
                                                          text2Label.frame.origin.y + text2Label.frame.size.height,
                                                          SCREEN_WIDTH - 30,
                                                          0)];
                [sender setTag:12];
            }
        }];
    }
}

#pragma mark - UserDefienedFunctions
- (void)popViewController{
    [[self navigationController]popViewControllerAnimated:YES];
}

- (void)saveOffer{
    if (!saved) {
        [offerDetailsModel saveOfferInDB:offer];
    }
    else {
        [offerDetailsModel removeOfferFromDB:offer];
    }
    saved = [offerDetailsModel checkIfOfferSaved:offer];
    if (saved) {
        //remove.from.favourite
        [self.saveButton setTitle:NSLocalizedString(@"remove.from.favourite", @"remove.from.favourite") forState:UIControlStateNormal];
    }else {
        //add.to.favourite
        [self.saveButton setTitle:NSLocalizedString(@"add.to.favourite", @"add.to.favourite") forState:UIControlStateNormal];
    }
}

- (void)useOfferOrShowCode {
    if (used)
    {
        OfferVCodeViewController *offerVCodeView = [[OfferVCodeViewController alloc]initWithNibName:@"OfferVCodeViewController" bundle:nil Offer:offer];
        [[self navigationController]pushViewController:offerVCodeView animated:YES];
    }
    else
    {
        [sharedDataObject setMainHUD:[MBProgressHUD showHUDAddedTo:[self view] animated:YES]];
        [ServerAPI useOfferWithID:ID UserID:@"0599123456" Success:^(id response) {
            
            [offer setObject:[response objectForKey:@"vcode"] forKey:@"offerVCode"];
            [offerDetailsModel saveOfferInDBWithCode:offer];
            
            used = YES;
            [self.useButton setTitle:@"اعرض الرمز" forState:UIControlStateNormal];
            
            OfferVCodeViewController *offerVCodeView = [[OfferVCodeViewController alloc]initWithNibName:@"OfferVCodeViewController" bundle:nil Offer:offer];
            [[self navigationController]pushViewController:offerVCodeView animated:YES];
            [[sharedDataObject mainHUD]hide:YES];
        } Failure:^(NSString *errorString) {
            //
            [[sharedDataObject mainHUD]hide:YES];
        }];

    }
    
}

#pragma mark - UserDefiendFunctions
- (void)rightSideMenuButtonPressed {
    //[[self menuContainerViewController]toggleRightSideMenuCompletion:^{}];
    [sharedDataObject.sideMenu toggleMenu];
}
@end

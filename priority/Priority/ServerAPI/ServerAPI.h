//
//  ServerAPI.h
//  Priority
//
//  Created by AnAs EiD on 4/20/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "SharedDataObject.h"

#define SERVER_ADDRESS @"http://priooffer.azurewebsites.net/api/"

#define kBackGroudQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

typedef void (^SuccessBlock) (id response);
typedef void (^FailureBlock) (NSString *errorString);

@interface ServerAPI : NSObject {
    
}
/**
 * Get Offers Categories
 *
 */
+ (void)getOffersCategoriesSuccess:(SuccessBlock)success
                           failure:(FailureBlock)failure;
/**
 * Send Customer Info to the server
 * @params (NSDictionary*) params
 */
+ (void)sendCustomerInfoWithParams:(NSDictionary *)params
                           Success:(SuccessBlock)success
                           failure:(FailureBlock)failure;
/**
 * Get Offer From the Server By ID
 * @params (NSInteger) offerID
 */
+ (void)getOfferWithID:(NSInteger)ID
               Success:(SuccessBlock)success
               failure:(FailureBlock)failure;
/**
 * Get Offers By Category ID
 * @params (NSInteger) categoryID
 */
+ (void)getOffersWithCategoryID:(NSInteger)ID
                        Success:(SuccessBlock)success
                        Failure:(FailureBlock)failure;
/**
 * Get Featured Offers From Server
 *
 */
+ (void)getFeaturedOffersWithSuccess:(SuccessBlock)success
                             Failure:(FailureBlock)failure;
/**
 * Search Offers By Keyword
 *
 */
+ (void)SearchOffersByKeyword:(NSString *)keyword
                      Success:(SuccessBlock)success
                      Failure:(FailureBlock)failure;
/**
 * Use This Function to Reserve an Offer on the server
 * @params (NSInteger)offerID ; (NSString)userID
 */
+ (void)useOfferWithID:(NSInteger)ID
                UserID:(NSString *)userID
               Success:(SuccessBlock)success
               Failure:(FailureBlock)failure;


#pragma mark - Requests

@end

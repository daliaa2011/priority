
//
//  ServerAPI.m
//  Priority
//
//  Created by AnAs EiD on 4/20/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "ServerAPI.h"
#import "AFHTTPRequestOperationManager.h"

@implementation ServerAPI

#pragma mark - requests
+ (void)getOffersCategoriesSuccess:(SuccessBlock)success
                           failure:(FailureBlock)failure{
    
    NSString *paramString = [NSString stringWithFormat:@"%@catsapi/GetCatsTBLs",SERVER_ADDRESS];
    
    NSString* encodedUrl = [paramString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:encodedUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([NSString stringWithFormat:@"Error : %@",error]);
    }];
}

+ (void)sendCustomerInfoWithParams:(NSDictionary *)params
                           Success:(SuccessBlock)success
                           failure:(FailureBlock)failure{
    
    NSString *paramString = [NSString stringWithFormat:@"%@CustomersApi/PostCustomersTBL",SERVER_ADDRESS];
    NSString* encodedUrl = [paramString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:encodedUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([NSString stringWithFormat:@"Error : %@",error]);
    }];
}

+ (void)getOfferWithID:(NSInteger)ID
               Success:(SuccessBlock)success
               failure:(FailureBlock)failure{
    NSString *paramString = @"";
    BOOL englishLang = [[NSUserDefaults standardUserDefaults] boolForKey:@"englishLang"];
    if (englishLang){
        paramString = [NSString stringWithFormat:@"%@OffersApien/GetOffersTBL?id=%ld",SERVER_ADDRESS,(long)ID];
    }
    else {
        paramString = [NSString stringWithFormat:@"%@OffersApi/GetOffersTBL?id=%ld",SERVER_ADDRESS,(long)ID];
    }
    
    NSString* encodedUrl = [paramString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:encodedUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([NSString stringWithFormat:@"Error : %@",error]);
    }];
}

+ (void)getOffersWithCategoryID:(NSInteger)ID
                        Success:(SuccessBlock)success
                        Failure:(FailureBlock)failure{
    
    NSString *paramString = @"";
    BOOL englishLang = [[NSUserDefaults standardUserDefaults] boolForKey:@"englishLang"];
    if (englishLang){
        paramString = [NSString stringWithFormat:@"%@OffersApien/GetOffersTBLsCat?id=%ld",SERVER_ADDRESS,(long)ID];
    }
    else {
        paramString = [NSString stringWithFormat:@"%@OffersApi/GetOffersTBLsCat?id=%ld",SERVER_ADDRESS,(long)ID];
    }
    NSString* encodedUrl = [paramString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:encodedUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([NSString stringWithFormat:@"Error : %@",error]);
    }];
}

+ (void)getFeaturedOffersWithSuccess:(SuccessBlock)success
                             Failure:(FailureBlock)failure{
   NSString *paramString = @"";
    BOOL englishLang = [[NSUserDefaults standardUserDefaults] boolForKey:@"englishLang"];
    if (englishLang) {
        paramString = [NSString stringWithFormat:@"%@OffersApien/GetOffersTBLsFeaturedOnline",SERVER_ADDRESS];
    }
    else {
        paramString = [NSString stringWithFormat:@"%@OffersApi/GetOffersTBLsFeaturedOnline",SERVER_ADDRESS];
    }

    NSString* encodedUrl = [paramString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:encodedUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([NSString stringWithFormat:@"Error : %@",error]);
    }];
}

+ (void)SearchOffersByKeyword:(NSString *)keyword
                      Success:(SuccessBlock)success
                      Failure:(FailureBlock)failure{
    NSString *paramString = [NSString stringWithFormat:@"%@OffersApi/GetOffersTBLsSearch?id=%@",SERVER_ADDRESS,keyword];
    NSString* encodedUrl = [paramString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:encodedUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([NSString stringWithFormat:@"Error : %@",error]);
    }];
}

+ (void)useOfferWithID:(NSInteger)ID
                UserID:(NSString *)userID
               Success:(SuccessBlock)success
               Failure:(FailureBlock)failure{
    //OffersApi/GetOfferSubscCustomer?id=%ld&CustomerID=%@
    NSString *paramString = [NSString stringWithFormat:@"%@OffersApi/GetOfferSubscCustomer?id=%ld&CustomerID=%@",SERVER_ADDRESS,(long)ID,userID];
    NSString* encodedUrl = [paramString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:encodedUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([NSString stringWithFormat:@"Error : %@",error]);
    }];
}

@end

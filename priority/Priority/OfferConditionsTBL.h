//
//  OfferConditionsTBL.h
//  Priority
//
//  Created by Dalia Mahmoud on 7/11/17.
//  Copyright © 2017 Arab Mobile Content. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfferConditionsTBL : NSObject
@property int _ID;
@property (retain, nonatomic) NSString *name;
@property (retain, nonatomic) NSString *NameEn;

@property (retain, nonatomic) NSString *details;
@property BOOL isActive;

@end

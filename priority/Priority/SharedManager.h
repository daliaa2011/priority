//
//  SharedManager.h
//  IslamAudio
//
//  Created by Dalia Abd El-Hadi on 10/21/15.
//  Copyright © 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SharedManager : NSObject
+ (SharedManager *) sharedInstance;

- (BOOL) validateEmail:(NSString*)email;
- (NSString*) getStringValue:(NSString*) str;

@end

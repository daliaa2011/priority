//
//  OfferDetailsModel.h
//  Priority
//
//  Created by AnAs EiD on 5/27/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "SharedDataObject.h"
#import "AppDataDelegate.h"

@interface OfferDetailsModel : NSObject{
    SharedDataObject *sharedDataObject;
}

- (NSString *)getOfferVCode:(int)offerID;
- (BOOL)saveOfferInDB:(NSMutableDictionary *)offer;
- (BOOL)saveOfferInDBWithCode:(NSMutableDictionary *)offer;
- (BOOL)checkIfOfferUsed:(NSMutableDictionary *)offer;
- (BOOL)checkIfOfferSaved:(NSMutableDictionary *)offer;
- (void)removeOfferFromDB:(NSMutableDictionary *)offer;
@end

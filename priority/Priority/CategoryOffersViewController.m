//
//  CategoryOffersViewController.m
//  Priority
//
//  Created by AnAs EiD on 4/29/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "CategoryOffersViewController.h"
#import "SearchViewController.h"
#import "btSimpleSideMenu.h"
#import "Priority-Swift.h"
#import "MainScreenModel.h"

@interface CategoryOffersViewController (){
    NSMutableArray *newestOffers;
    NSMutableArray *nearestOffers;
    int _categoryID;
    
    BOOL newest;
    BOOL nearest;
    
//    double oldOffset;
//    double newOffset;
}

@end

@implementation CategoryOffersViewController

- (SharedDataObject *)theAppDataObject{
    id<AppDataDelegate>delegate = (id<AppDataDelegate>)[UIApplication sharedApplication].delegate;
    SharedDataObject *dataObject;
    dataObject = (SharedDataObject *)delegate.theSharedDataObject;
    return dataObject;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil CategoryID:(NSInteger)categoryID{
    self = [super init];
    if (self) {
        sharedDataObject = [self theAppDataObject];
        _categoryID = (int)categoryID;
        newest = YES;
        nearest = NO;
        //get Offers by Category
        [ServerAPI getOffersWithCategoryID:categoryID
                                   Success:^(id response) {
                                       newestOffers = [NSMutableArray array];
                                       nearestOffers = [NSMutableArray array];
                                       for (NSMutableArray *rawOffer in response) {
                                           [newestOffers addObject:[[OfferObject alloc]createArrayObjectWithResponce:rawOffer]];
                                           [nearestOffers addObject:[[OfferObject alloc]createArrayObjectWithResponce:rawOffer]];
                                       }
                                       [self sortOffersArray];
                                       [_categoryOffersTableView reloadData];
                                       [[sharedDataObject mainHUD]setHidden:YES];
                                   }
                                   Failure:^(NSString *errorString) {
                                       NSLog(@"Error : %@",errorString);
                                   }];
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view localizeSubViews];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    /* Init table header view by using image or image from url*/
    //self.headerView = [[DTParallaxHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH * 187.0/320) withImage:[UIImage imageNamed:@"foodDrink"] withTabBar:self.buttonsView];
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, (SCREEN_WIDTH - 20) * 167.0/300 + 60);
    self.headerView.tabBarView = self.buttonsView;
    self.headerView.clipsToBounds = YES;
//    [headerView addSubview:self.headerView];
//    self.headerView.frame = headerView.bounds;
//    self.headerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    DTHeaderView *headerView = [[DTHeaderView alloc] initWithFrame:CGRectMake(0, 0, 320, 200) withImageUrl:@"http://s3.favim.com/orig/47/colorful-fun-girl-night-ocean-Favim.com-437603.jpg" withTabBar:tabbar];
    
    [self.categoryOffersTableView setDTHeaderView:self.headerView];
    self.categoryOffersTableView.showShadow = NO;
    
    
    self.categoryOffersTableView.backgroundColor = [UIColor clearColor];
    [self.categoryOffersTableView registerNib:[UINib nibWithNibName:@"OfferCustomTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"offerCell"];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    titleLabel.text = NSLocalizedString(@"priority", @"priority");
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi" size:16.0];
    titleLabel.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabel;
    
    
    UIButton *rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 20, 20)];
    [rightButton setImage:[UIImage imageNamed:@"Menu"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    [[self navigationItem]setRightBarButtonItem:rightBarButton];
    
    UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 20, 20)];
    [leftButton setImage:[UIImage imageNamed:@"Search"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(searchOffers) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    [[self navigationItem]setLeftBarButtonItem:leftBarButton];
    
//    [[self menuContainerViewController]setPanMode:MFSideMenuPanModeNone];
    
    
    self.newestButton.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.newestButton.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.newestButton.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    
    self.nearestButton.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.nearestButton.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.nearestButton.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    
    [sharedDataObject setMainHUD:[MBProgressHUD showHUDAddedTo:_categoryOffersTableView animated:YES]];
    [[sharedDataObject mainHUD]setDimBackground:YES];
    
    if ([_categoryOffersTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_categoryOffersTableView setLayoutMargins:UIEdgeInsetsZero];
    }
    MainScreenModel *mainScreenModel = [[MainScreenModel alloc]init];

    [_categoryNameLabel setText:[mainScreenModel getCategoryNameForID:_categoryID]];
    switch (_categoryID) {
        case 1:
        {
            [_categoryCoverImageView setImage:[UIImage imageNamed:@"foodDrink"]];
        }
            break;
        case 2:
        {
            [_categoryCoverImageView setImage:[UIImage imageNamed:@"shopping"]];
        }
            break;
        case 3:
        {
            [_categoryCoverImageView setImage:[UIImage imageNamed:@"travel"]];
        }
            break;
        case 4:
        {
            [_categoryCoverImageView setImage:[UIImage imageNamed:@"health-and-beauty"]];
        }
            break;
        case 5:
        {
            [_categoryCoverImageView setImage:[UIImage imageNamed:@"entertainment"]];
        }
            break;
        default:
            break;
    }
//    oldOffset = 0.0;
//    self.tabelViewLayoutConstraint.constant = SCREEN_HEIGHT - 105;
//    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT + 190)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//#pragma mark - UIScrollViewDelegate
//
//- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
//    if (scrollView == _categoryOffersTableView) {
//        if (oldOffset < scrollView.contentOffset.y){
//            [_mainScrollView setContentOffset:CGPointMake(0.0, 190.0) animated:YES];
//            oldOffset = scrollView.contentOffset.y;
//        }
//    }
//}
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    if (scrollView == _categoryOffersTableView) {
//        if (scrollView.contentOffset.y == 0) {
//            [_mainScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
//            oldOffset = 0;
//        }
//    }
//}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"offerCell";
    OfferCustomTableViewCell *offerCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    offerCell.backgroundColor = [UIColor clearColor];
    offerCell.contentView.backgroundColor = [UIColor clearColor];
    
    if (newest) {
        
        offerCell.offerDetailsLabel.text = [[newestOffers objectAtIndex:[newestOffers count] - indexPath.row - 1] objectForKey:@"offerDetails"];
//        offerCell.businessNameLabel.text = [[newestOffers objectAtIndex:[newestOffers count] - indexPath.row - 1] objectForKey:@"businessName"];
        
        [[offerCell offerTitleLabel]setText:[[newestOffers objectAtIndex:[newestOffers count] - indexPath.row - 1] objectForKey:@"Title"]] ;
        [[offerCell distanceLabel]setText:[NSString stringWithFormat:@"%0.2f كم",[[[newestOffers objectAtIndex:[newestOffers count] - indexPath.row - 1]objectForKey:@"businessDistance"] floatValue]/1000.0]];
        
        if ([[newestOffers objectAtIndex:[newestOffers count] - indexPath.row - 1] objectForKey:@"businessLogo"]) {
            __block UIActivityIndicatorView *activityIndicator;
            __weak UIImageView *weakImageView = [offerCell logoImageView];
            [[offerCell logoImageView]sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/company/%@",[[newestOffers objectAtIndex:[newestOffers count] - indexPath.row - 1]objectForKey:@"businessLogo"]]]
                                                            andPlaceholderImage:nil
                                                                        options:SDWebImageProgressiveDownload
                                                                       progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               if (!activityIndicator) {
                                                                                   [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                                   [activityIndicator setCenter:[weakImageView center]];
                                                                                   [activityIndicator startAnimating];
                                                                               }
                                                                           });
                                                                           
                                                                       } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [activityIndicator removeFromSuperview];
                                                                               activityIndicator = nil;
                                                                               if (error) {
                                                                                   //NSLog(@"error : %@",error);
                                                                               }
                                                                           });
                                                                           
                                                                       }];
        }
        if ([[newestOffers objectAtIndex:[newestOffers count] - indexPath.row - 1] objectForKey:@"offerLogo"]) {
            __block UIActivityIndicatorView *activityIndicator;
            __weak UIImageView *weakImageView = [offerCell offerImageView];
            [[offerCell offerImageView]sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/offers/%@",[[newestOffers objectAtIndex:[newestOffers count] - indexPath.row - 1]objectForKey:@"offerLogo"]]]
                                                             andPlaceholderImage:[UIImage imageNamed:@"placeholders"]
                                                                         options:SDWebImageProgressiveDownload
                                                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                if (!activityIndicator) {
                                                                                    [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                                    [activityIndicator setCenter:[weakImageView center]];
                                                                                    [activityIndicator startAnimating];
                                                                                }
                                                                            });
                                                                            
                                                                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                [activityIndicator removeFromSuperview];
                                                                                activityIndicator = nil;
                                                                                if (error) {
                                                                                    //NSLog(@"error : %@",error);
                                                                                }
                                                                            });
                                                                            
                                                                        }];
        }
    }
    
    else if (nearest) {
        
        offerCell.offerDetailsLabel.text = [[nearestOffers objectAtIndex:indexPath.row] objectForKey:@"offerDetails"];
        offerCell.businessNameLabel.text = [[nearestOffers objectAtIndex:indexPath.row]  objectForKey:@"businessName"];
        
        [[offerCell distanceLabel]setText:[NSString stringWithFormat:@"%@",[[nearestOffers objectAtIndex:indexPath.row] objectForKey:@"businessDistance"]]];
        
        [[offerCell offerTitleLabel]setText:[[nearestOffers objectAtIndex:indexPath.row] objectForKey:@"offerName"]] ;
        [[offerCell distanceLabel]setText:[NSString stringWithFormat:@"%0.2f كم",[[[nearestOffers objectAtIndex:indexPath.row] objectForKey:@"businessDistance"] floatValue]/1000.0]];
        
        if ([[nearestOffers objectAtIndex:indexPath.row] objectForKey:@"businessLogo"]) {
            __block UIActivityIndicatorView *activityIndicator;
            __weak UIImageView *weakImageView = [offerCell logoImageView];
            [[offerCell logoImageView]sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/company/%@",[[nearestOffers objectAtIndex:indexPath.row]objectForKey:@"businessLogo"]]]
                                                            andPlaceholderImage:nil
                                                                        options:SDWebImageProgressiveDownload
                                                                       progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               if (!activityIndicator) {
                                                                                   [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                                   [activityIndicator setCenter:[weakImageView center]];
                                                                                   [activityIndicator startAnimating];
                                                                               }
                                                                           });
                                                                           
                                                                       } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [activityIndicator removeFromSuperview];
                                                                               activityIndicator = nil;
                                                                               if (error) {
                                                                                   //NSLog(@"error : %@",error);
                                                                               }
                                                                           });
                                                                           
                                                                       }];
        }
        if ([[nearestOffers objectAtIndex:indexPath.row] objectForKey:@"offerLogo"]) {
            __block UIActivityIndicatorView *activityIndicator;
            __weak UIImageView *weakImageView = [offerCell offerImageView];
            [[offerCell offerImageView]sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/offers/%@",[[nearestOffers objectAtIndex:indexPath.row]objectForKey:@"offerLogo"]]]
                                                             andPlaceholderImage:[UIImage imageNamed:@"placeholders"]
                                                                         options:SDWebImageProgressiveDownload
                                                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                if (!activityIndicator) {
                                                                                    [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                                    [activityIndicator setCenter:[weakImageView center]];
                                                                                    [activityIndicator startAnimating];
                                                                                }
                                                                            });
                                                                            
                                                                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                [activityIndicator removeFromSuperview];
                                                                                activityIndicator = nil;
                                                                                if (error) {
                                                                                    //NSLog(@"error : %@",error);
                                                                                }
                                                                            });
                                                                            
                                                                        }];
        }
    }
    
    if ([offerCell respondsToSelector:@selector(setLayoutMargins:)]) {
        [offerCell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    [offerCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return offerCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [newestOffers count];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    OfferDetailsViewController *OfferDetailsView;
    if (newest) {
        OfferDetailsView = [[OfferDetailsViewController alloc]initWithNibName:@"OfferDetailsViewController" bundle:nil OfferID:[[[newestOffers objectAtIndex:[indexPath row]]objectForKey:@"offerID"] intValue]];
    }
    else if (nearest){
        OfferDetailsView = [[OfferDetailsViewController alloc]initWithNibName:@"OfferDetailsViewController" bundle:nil OfferID:[[[nearestOffers objectAtIndex:[indexPath row]]objectForKey:@"offerID"] intValue]];
    }
    [[self navigationController]pushViewController:OfferDetailsView animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat screenwidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = ((screenwidth - 20)/2 * (121.0/150)) + 10;
    return height;
}

- (void)popViewController{
    [[self navigationController]popViewControllerAnimated:YES];
}

- (void)sortOffersArray {
    for (int i = 0; i < [nearestOffers count]; i++) {
        [[nearestOffers objectAtIndex:i]setObject:[NSNumber numberWithInt:[[sharedDataObject currentLocation] distanceFromLocation:[[nearestOffers objectAtIndex:i]objectForKey:@"businessLocation"]]] forKey:@"businessDistance"];
        
        [[newestOffers objectAtIndex:i]setObject:[NSNumber numberWithInt:[[sharedDataObject currentLocation] distanceFromLocation:[[newestOffers objectAtIndex:i]objectForKey:@"businessLocation"]]] forKey:@"businessDistance"];
    }
    [nearestOffers sortUsingDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"businessDistance" ascending:YES], nil]];
}

- (IBAction)showNearestOffers:(id)sender {
    self.nearestButton.backgroundColor = UIColorFromRGB(0xf1583f);
    self.newestButton.backgroundColor = UIColorFromRGB(0x222222);
    
    nearest = YES;
    newest = NO;
    [_categoryOffersTableView reloadData];
}

- (IBAction)showNewestOffers:(id)sender {
    self.nearestButton.backgroundColor = UIColorFromRGB(0x222222);
    self.newestButton.backgroundColor = UIColorFromRGB(0xf1583f);
    
    nearest = NO;
    newest = YES;
    [_categoryOffersTableView reloadData];
}

#pragma mark - UserDefiendFunctions
- (void)rightSideMenuButtonPressed {
    [sharedDataObject.sideMenu toggleMenu];
}

- (void)searchOffers {
    SearchViewController *searchView = [[SearchViewController alloc]initWithNibName:@"SearchViewController" bundle:nil];
    [[self navigationController]pushViewController:searchView animated:YES];
}
@end

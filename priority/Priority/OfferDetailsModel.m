//
//  OfferDetailsModel.m
//  Priority
//
//  Created by AnAs EiD on 5/27/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "OfferDetailsModel.h"

@implementation OfferDetailsModel

- (SharedDataObject *)theAppDataObject{
    id<AppDataDelegate>delegate = (id<AppDataDelegate>)[UIApplication sharedApplication].delegate;
    SharedDataObject *dataObject;
    dataObject = (SharedDataObject *)delegate.theSharedDataObject;
    return dataObject;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        sharedDataObject = [self theAppDataObject];
    }
    return self;
}

- (BOOL)saveOfferInDBWithCode:(NSMutableDictionary *)offer{
    sharedDataObject = [[SharedDataObject alloc]init];
    CLLocation *location = [offer objectForKey:@"businessLocation"];
    BOOL result = YES;
    [sharedDataObject set_DBObject:[[PriorityDAO alloc] init]];
    [[sharedDataObject _DBObject]connectDB];
    NSString *query;
    
    int ID = [[sharedDataObject _DBObject]getIntegerValue:[[NSString stringWithFormat:@"SELECT COUNT(ID) FROM BusinessTBL WHERE ID = '%@'",[offer objectForKey:@"businessID"]] UTF8String]];
    if (!ID) {
        query = [[NSString alloc]initWithFormat:@"INSERT INTO BusinessTBL (Address,CategoryID,City,CountryID,Details,ID,Latitude,Logo,Longitude,Name) VALUES ('%@',%ld,'%@',%ld,'%@','%@','%@','%@','%@','%@')",
                 [offer objectForKey:@"businessAddress"],
                 (long)[[offer objectForKey:@"CatsTBLID"] integerValue],
                 [offer objectForKey:@"businessCity"],
                 (long)[[offer objectForKey:@"CountryId"] integerValue],
                 [offer objectForKey:@"businessDetails"],
                 [offer objectForKey:@"businessID"],
                 [NSString stringWithFormat:@"%f",location.coordinate.latitude],
                 [offer objectForKey:@"businessLogo"],
                 [NSString stringWithFormat:@"%f",location.coordinate.longitude],
                 [offer objectForKey:@"businessName"]];
        [[sharedDataObject _DBObject]insertData:query];
    }
    //INSERT INTO OfferTBL (BussinessesTBLID,CodeType,Details,EndDate,EndTime,ID,Logo,Name,StartDate,StartTime,VCode) VALUES ('%@',%ld,'%@','%@','%@'%ld,'%@','%@','%@','%@','%@')
    //[[offer objectForKey:@"offerID"] integerValue]
    
    query = [[NSString alloc]initWithFormat:@"INSERT INTO OfferTBL (BussinessesTBLID,CodeType,Details,EndDate,EndTime,ID,Logo,Name,StartDate,StartTime,VCode,Cond1,Cond2,Cond3,Cond4,Cond5,Cond6,Cond7,ExtraCond1,ExtraCond2,ExtraCond3,Sat,Sun,Mon,Tue,Wed,Thr,Fri) VALUES ('%@',%ld,'%@','%@','%@',%ld,'%@','%@','%@','%@','%@','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d')",
             [offer objectForKey:@"businessID"],
             (long)[[offer objectForKey:@"offerCodeType"]integerValue],
             [offer objectForKey:@"offerDetails"],
             [offer objectForKey:@"offerEndDate"],
             [offer objectForKey:@"offerEndTime"],
             (long)[[offer objectForKey:@"offerID"] integerValue],
             [offer objectForKey:@"offerLogo"],
             [offer objectForKey:@"offerName"],
             [offer objectForKey:@"offerStartDate"],
             [offer objectForKey:@"offerStartTime"],
             [offer objectForKey:@"offerVCode"],
             [[offer objectForKey:@"Cond1"] intValue],
             [[offer objectForKey:@"Cond2"] intValue],
             [[offer objectForKey:@"Cond3"] intValue],
             [[offer objectForKey:@"Cond4"] intValue],
             [[offer objectForKey:@"Cond5"] intValue],
             [[offer objectForKey:@"Cond6"] intValue],
             [[offer objectForKey:@"Cond7"] intValue],
             [[offer objectForKey:@"ExtraCond1"] intValue],
             [[offer objectForKey:@"ExtraCond2"] intValue],
             [[offer objectForKey:@"ExtraCond3"] intValue],
             [[offer objectForKey:@"Sat"] intValue],
             [[offer objectForKey:@"Sun"] intValue],
             [[offer objectForKey:@"Mon"] intValue],
             [[offer objectForKey:@"Tue"] intValue],
             [[offer objectForKey:@"Wed"] intValue],
             [[offer objectForKey:@"Thr"] intValue],
             [[offer objectForKey:@"Fri"] intValue]];
    [[sharedDataObject _DBObject]insertData:query];
    [[sharedDataObject _DBObject]disConnectDB];
    return result;
}

- (void)removeOfferFromDB:(NSMutableDictionary *)offer {
    sharedDataObject = [[SharedDataObject alloc]init];
    [sharedDataObject set_DBObject:[[PriorityDAO alloc] init]];
    [[sharedDataObject _DBObject]connectDB];
    NSString *query = [NSString stringWithFormat:@"Delete from OfferTBL where ID = %d",(long)[[offer objectForKey:@"offerID"] integerValue]];
    [[sharedDataObject _DBObject]insertData:query];
    [[sharedDataObject _DBObject]disConnectDB];
}

- (BOOL)saveOfferInDB:(NSMutableDictionary *)offer{
    sharedDataObject = [[SharedDataObject alloc]init];
    CLLocation *location = [offer objectForKey:@"businessLocation"];
    BOOL result = YES;
    [sharedDataObject set_DBObject:[[PriorityDAO alloc] init]];
    [[sharedDataObject _DBObject]connectDB];
    NSString *query;
    
    int ID = [[sharedDataObject _DBObject]getIntegerValue:[[NSString stringWithFormat:@"SELECT COUNT(ID) FROM BusinessTBL WHERE ID = '%@'",[offer objectForKey:@"businessID"]] UTF8String]];
    if (!ID) {
        query = [[NSString alloc]initWithFormat:@"INSERT INTO BusinessTBL (Address,CategoryID,City,CountryID,Details,ID,Latitude,Logo,Longitude,Name) VALUES ('%@',%ld,'%@',%ld,'%@','%@','%@','%@','%@','%@')",
                 [offer objectForKey:@"businessAddress"],
                 (long)[[offer objectForKey:@"CatsTBLID"] integerValue],
                 [offer objectForKey:@"businessCity"],
                 (long)[[offer objectForKey:@"CountryId"] integerValue],
                 [offer objectForKey:@"businessDetails"],
                 [offer objectForKey:@"businessID"],
                 [NSString stringWithFormat:@"%f",location.coordinate.latitude],
                 [offer objectForKey:@"businessLogo"],
                 [NSString stringWithFormat:@"%f",location.coordinate.longitude],
                 [offer objectForKey:@"businessName"]];
        [[sharedDataObject _DBObject]insertData:query];
    }

    query = [[NSString alloc]initWithFormat:@"INSERT INTO OfferTBL (BussinessesTBLID,CodeType,Details,EndDate,EndTime,ID,Logo,Name,StartDate,StartTime,VCode,Cond1,Cond2,Cond3,Cond4,Cond5,Cond6,Cond7,ExtraCond1,ExtraCond2,ExtraCond3,Sat,Sun,Mon,Tue,Wed,Thr,Fri) VALUES ('%@',%ld,'%@','%@','%@',%ld,'%@','%@','%@','%@','-','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d')",
             [offer objectForKey:@"businessID"],
             (long)[[offer objectForKey:@"offerCodeType"]integerValue],
             [offer objectForKey:@"offerDetails"],
             [offer objectForKey:@"offerEndDate"],
             [offer objectForKey:@"offerEndTime"],
             (long)[[offer objectForKey:@"offerID"] integerValue],
             [offer objectForKey:@"offerLogo"],
             [offer objectForKey:@"offerName"],
             [offer objectForKey:@"offerStartDate"],
             [offer objectForKey:@"offerStartTime"],
             [[offer objectForKey:@"Cond1"] intValue],
             [[offer objectForKey:@"Cond2"] intValue],
             [[offer objectForKey:@"Cond3"] intValue],
             [[offer objectForKey:@"Cond4"] intValue],
             [[offer objectForKey:@"Cond5"] intValue],
             [[offer objectForKey:@"Cond6"] intValue],
             [[offer objectForKey:@"Cond7"] intValue],
             [[offer objectForKey:@"ExtraCond1"] intValue],
             [[offer objectForKey:@"ExtraCond2"] intValue],
             [[offer objectForKey:@"ExtraCond3"] intValue],
             [[offer objectForKey:@"Sat"] intValue],
             [[offer objectForKey:@"Sun"] intValue],
             [[offer objectForKey:@"Mon"] intValue],
             [[offer objectForKey:@"Tue"] intValue],
             [[offer objectForKey:@"Wed"] intValue],
             [[offer objectForKey:@"Thr"] intValue],
             [[offer objectForKey:@"Fri"] intValue]];
    [[sharedDataObject _DBObject]insertData:query];
    [[sharedDataObject _DBObject]disConnectDB];
    return result;
}

- (BOOL)checkIfOfferUsed:(NSMutableDictionary *)offer {
    BOOL isUsed = NO;
    //[[NSString stringWithFormat:@"SELECT COUNT(ID) FROM OfferTBL WHERE ID = '%@'",[offer objectForKey:@"offerID"]] UTF8String]];
    [sharedDataObject set_DBObject:[[PriorityDAO alloc] init]];
    [[sharedDataObject _DBObject]connectDB];
    isUsed = [[sharedDataObject _DBObject]getIntegerValue:[[NSString stringWithFormat:@"SELECT COUNT(ID) FROM OfferTBL WHERE ID = '%@' AND vcode <> '-'",[offer objectForKey:@"offerID"]] UTF8String]];
    [[sharedDataObject _DBObject]disConnectDB];
    return isUsed;
}

- (BOOL)checkIfOfferSaved:(NSMutableDictionary *)offer {
    BOOL isUsed = NO;
    //[[NSString stringWithFormat:@"SELECT COUNT(ID) FROM OfferTBL WHERE ID = '%@'",[offer objectForKey:@"offerID"]] UTF8String]];
    [sharedDataObject set_DBObject:[[PriorityDAO alloc] init]];
    [[sharedDataObject _DBObject]connectDB];
    isUsed = [[sharedDataObject _DBObject]getIntegerValue:[[NSString stringWithFormat:@"SELECT COUNT(ID) FROM OfferTBL WHERE ID = '%@' AND vcode = '-'",[offer objectForKey:@"offerID"]] UTF8String]];
    [[sharedDataObject _DBObject]disConnectDB];
    return isUsed;
}

- (NSString *)getOfferVCode:(int)offerID{
    [sharedDataObject set_DBObject:[[PriorityDAO alloc] init]];
    [[sharedDataObject _DBObject]connectDB];
    NSString *vCode = [[sharedDataObject _DBObject]getDataText:[[NSString stringWithFormat:@"SELECT vcode FROM OfferTBL WHERE ID = %d",offerID] UTF8String]];
    return vCode;
}
@end

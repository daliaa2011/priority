//
//  OfferDetailsViewController.h
//  Priority
//
//  Created by AnAs EiD on 5/4/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "AppDataDelegate.h"
#import "SharedDataObject.h"

#import "OfferDetailsModel.h"
#include "OfferVCodeViewController.h"

@interface OfferDetailsViewController : UIViewController<CLLocationManagerDelegate>{
    SharedDataObject *sharedDataObject;
    OfferDetailsModel *offerDetailsModel;
    int ID;
}

@property (retain, nonatomic) IBOutlet UIScrollView *mainScrollView;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil OfferID:(int)offerID;





@property (strong, nonatomic) IBOutlet UIImageView *offerImageImageView;
@property (strong, nonatomic) IBOutlet UIImageView * offerLogoImageView;
@property (strong, nonatomic) IBOutlet UILabel *offerTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *offerCategoryLabel;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *offerShortDesLabel;
@property (strong, nonatomic) IBOutlet UILabel *offerDetailsLabel;

@property (strong, nonatomic) IBOutlet UILabel *businessNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UIView *mapContainerView;

@property (strong, nonatomic) IBOutlet UIButton* useButton;
@property (strong, nonatomic) IBOutlet UIButton* saveButton;

@property (strong, nonatomic) IBOutlet UILabel *restrictionsLabel;
@property (strong, nonatomic) IBOutlet UILabel *conditionsLabel;
@end

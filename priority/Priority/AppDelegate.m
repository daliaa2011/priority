//
//  AppDelegate.m
//  Priority
//
//  Created by AnAs EiD on 4/12/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "AppDelegate.h"
#import "btSimpleSideMenu.h"
#import "Priority-Swift.h"

@implementation AppDelegate

- (id)init
{
    self.theSharedDataObject = [[SharedDataObject alloc]init];
    return self;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //Create Editable Copy of Database
    [PriorityDAO createEditableCopyOfDatabaseIfNeeded];
    
    // Override point for customization after application launch.
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    BOOL englishLang = [[NSUserDefaults standardUserDefaults] boolForKey:@"englishLang"];
    if (englishLang || ![[NSUserDefaults standardUserDefaults] valueForKey:@"englishLang"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"englishLang"];

        [L102Language setAppleLAnguageToLang:@"en"];
        
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        [[UITextField appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        
    }
    else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"englishLang"];
        [L102Language setAppleLAnguageToLang:@"ar"];
        
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        [[UITextField appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    [L102Localizer DoTheMagic];

    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    _mainScreenView = [[MainScreenViewController alloc]init];
    
    _navigationController = [[UINavigationController alloc]initWithRootViewController:self.mainScreenView];
    self.theSharedDataObject.sideMenu = [[BTSimpleSideMenu alloc] initAndaddToViewController:_navigationController];
    self.theSharedDataObject.sideMenu.delegate = self.mainScreenView;
    
    
    
    
    //Geo Services
    [GMSServices provideAPIKey:@"AIzaSyCMrK6CnPBrUZZ8PRMed_GD58oGwb2j5Zg"];
    
    
    [[[SharedDataObject alloc] init] loadCategories];
    
    self.window.rootViewController = _navigationController;
    [self.window makeKeyAndVisible];
    return YES;
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

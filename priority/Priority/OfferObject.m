//
//  OfferObject.m
//  Priority
//
//  Created by AnAs EiD on 5/20/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "OfferObject.h"
#import "SharedManager.h"

@implementation OfferObject

- (SharedDataObject *)theAppDataObject{
    id<AppDataDelegate>delegate = (id<AppDataDelegate>)[UIApplication sharedApplication].delegate;
    SharedDataObject *dataObject;
    dataObject = (SharedDataObject *)delegate.theSharedDataObject;
    return dataObject;
}

- (instancetype)initWithResponce:(id)response{
    self = [super init];
    if (self) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
        
        //Init Offer Object
        _businessAddress = [[NSString alloc]initWithFormat:@"%@ \n %@",[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Address1"],[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Address2"]];
        _CatsTBLID = [[self getStringValue:[[response objectForKey:@"BusinessesTBLs"]objectForKey:@"CatsTBLID"]] intValue];
        _businessCity = [[response objectForKey:@"BusinessesTBLs"] objectForKey:@"City"];
        _CountryId = [[self getStringValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"CountryId"]] intValue];
        _businessDetails = [[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Details"];
        _businessDistance = 0;
        _businessID = [[response objectForKey:@"BusinessesTBLs"] objectForKey:@"ID"];
        _businessLocation = [[CLLocation alloc] initWithLatitude:[[self getStringValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Latitude"]] doubleValue] longitude:[[self getStringValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Longitude"]] doubleValue]];
        _businessName = [[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Name"];
        _offerDetails = [response objectForKey:@"Details"];
        _offerEndDate = [dateFormatter dateFromString:[response objectForKey:@"EndDate"]];
        _offerEndTime = [dateFormatter dateFromString:[response objectForKey:@"EndTime"]];
        _offerGetFree = [response objectForKey:@"GetFree"];
        _offerName = [response objectForKey:@"Name"];
        _offerNum = [response objectForKey:@"OfferNum"];
        _offerStartDate = [dateFormatter dateFromString:[response objectForKey:@"StartDate"]];
        _offerStartTime = [dateFormatter dateFromString:[response objectForKey:@"StartTime"]];
        _offerType = [response objectForKey:@"OfferTypesTBLID"];
        _offerVCode = [response objectForKey:@"VCode"];
        _title = [response objectForKey:@"Title"];
        _OfferDesc = [response objectForKey:@"OfferDesc"];
    }
    return self;
}

- (NSMutableDictionary *)createArrayObjectWithResponce:(id)response {
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    
    //Create Offer Dictionary
    NSMutableDictionary *offerObject = [NSMutableDictionary dictionary];
    [offerObject setValue:[[NSString alloc]initWithFormat:@"%@ \n %@",[[SharedManager sharedInstance] getStringValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Address1"]],[[SharedManager sharedInstance] getStringValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Address2"]]]
                   forKey:@"businessAddress"];
    
    [offerObject setValue:[NSNumber numberWithInt:[[self getStringValue:[[response objectForKey:@"BusinessesTBLs"]objectForKey:@"CatsTBLID"]] intValue]]
                   forKey:@"CatsTBLID"];
    
    [offerObject setValue:[[SharedManager sharedInstance] getStringValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"City"]]
                   forKey:@"businessCity"];
    
    [offerObject setValue:[NSNumber numberWithInt:[[self getStringValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"CountryId"]] intValue]]
                   forKey:@"CountryId"];
    
    [offerObject setValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Details"]
                   forKey:@"businessDetails"];
    
    [offerObject setValue:[NSNumber numberWithInt:0]
                   forKey:@"businessDistance"];
    
    [offerObject setValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"ID"]
                   forKey:@"businessID"];
    
    [offerObject setValue:[[CLLocation alloc] initWithLatitude:[[self getStringValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Latitude"]] doubleValue] longitude:[[self getStringValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Longitude"]] doubleValue]]
                   forKey:@"businessLocation"];

    [offerObject setValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Logo"]
                   forKey:@"businessLogo"];
    
    [offerObject setValue:[[response objectForKey:@"BusinessesTBLs"] objectForKey:@"Name"]
                   forKey:@"businessName"];
    
    [offerObject setObject:[NSNumber numberWithInt:[[self getStringValue:[response objectForKey:@"OfferNum"]] intValue]] forKey:@"offerNum"];
    
    [offerObject setValue:[response objectForKey:@"CodeType"]
                   forKey:@"offerCodeType"];
    
    [offerObject setValue:[self getStringValue:[response objectForKey:@"Details"]]
                   forKey:@"offerDetails"];
    
    [offerObject setValue:[dateFormatter dateFromString:[response objectForKey:@"EndDate"]]
                   forKey:@"offerEndDate"];
    
    [offerObject setValue:[dateFormatter dateFromString:[response objectForKey:@"EndTime"]]
                   forKey:@"offerEndTime"];
    
    [offerObject setValue:[NSNumber numberWithInt:[[self getStringValue:[response objectForKey:@"GetFree"]] intValue]]
                   forKey:@"offerGetFree"];
    
    [offerObject setValue:[response objectForKey:@"ID"]
                   forKey:@"offerID"];
    
    [offerObject setValue:[response objectForKey:@"Logo"]
                   forKey:@"offerLogo"];
    
    [offerObject setValue:[response objectForKey:@"Name"]
                   forKey:@"offerName"];
    
    [offerObject setValue:[dateFormatter dateFromString:[response objectForKey:@"StartDate"]]
                   forKey:@"offerStartDate"];
    
    [offerObject setValue:[dateFormatter dateFromString:[response objectForKey:@"StartTime"]]
                   forKey:@"offerStartTime"];
    
    [offerObject setValue:[NSNumber numberWithInt:[[self getStringValue:[response objectForKey:@"OfferTypesTBLID"]] intValue]]
                   forKey:@"offerType"];
    
    [offerObject setValue:[response objectForKey:@"VCode"]
                   forKey:@"offerVCode"];
    
    [offerObject setValue:[response objectForKey:@"Cond1"]
                   forKey:@"Cond1"];
    [offerObject setValue:[response objectForKey:@"Cond2"]
                   forKey:@"Cond2"];
    [offerObject setValue:[response objectForKey:@"Cond3"]
                   forKey:@"Cond3"];
    [offerObject setValue:[response objectForKey:@"Cond4"]
                   forKey:@"Cond4"];
    [offerObject setValue:[response objectForKey:@"Cond5"]
                   forKey:@"Cond5"];
    [offerObject setValue:[response objectForKey:@"Cond6"]
                   forKey:@"Cond6"];
    [offerObject setValue:[response objectForKey:@"Cond7"]
                   forKey:@"Cond7"];
    [offerObject setValue:[response objectForKey:@"ExtraCond1"]
                   forKey:@"ExtraCond1"];
    [offerObject setValue:[response objectForKey:@"ExtraCond2"]
                   forKey:@"ExtraCond2"];
    [offerObject setValue:[response objectForKey:@"ExtraCond3"]
                   forKey:@"ExtraCond3"];
    
    [offerObject setValue:[response objectForKey:@"Sat"]
                   forKey:@"Sat"];
    [offerObject setValue:[response objectForKey:@"Sun"]
                   forKey:@"Sun"];
    [offerObject setValue:[response objectForKey:@"Mon"]
                   forKey:@"Mon"];
    [offerObject setValue:[response objectForKey:@"Tue"]
                   forKey:@"Tue"];
    [offerObject setValue:[response objectForKey:@"Wed"]
                   forKey:@"Wed"];
    [offerObject setValue:[response objectForKey:@"Thr"]
                   forKey:@"Thr"];
    [offerObject setValue:[response objectForKey:@"Fri"]
                   forKey:@"Fri"];
    
    
    [offerObject setValue:[response objectForKey:@"Title"]
                   forKey:@"Title"];
    [offerObject setValue:[response objectForKey:@"OfferDesc"]
                   forKey:@"OfferDesc"];
    [offerObject setValue:[response objectForKey:@"OfferConditionsTBLs"] forKey:@"OfferConditionsTBLs"];
        
//    
//    [offerObject setValue:[NSNumber numberWithInt:1]
//                   forKey:@"Cond1"];
//    [offerObject setValue:[NSNumber numberWithInt:1]
//                   forKey:@"Cond2"];
//    [offerObject setValue:[response objectForKey:@"Cond3"]
//                   forKey:@"Cond3"];
//    [offerObject setValue:[NSNumber numberWithInt:1]
//                   forKey:@"Cond4"];
//    [offerObject setValue:[response objectForKey:@"Cond5"]
//                   forKey:@"Cond5"];
//    [offerObject setValue:[response objectForKey:@"Cond6"]
//                   forKey:@"Cond6"];
//    [offerObject setValue:[response objectForKey:@"Cond7"]
//                   forKey:@"Cond7"];
    return offerObject;
}

- (NSString *)getOfferCategoryName:(int)typeID {
    SharedDataObject *sharedDataObject = [self theAppDataObject];
    return [[sharedDataObject _DBObject]getDataText:[[NSString stringWithFormat:@"SELECT Name FROM OfferTypeTBL WHERE ID = %d",typeID] UTF8String]];;
}

- (NSString*) getStringValue:(NSString*) str
{
    if (!str || [str isEqual:[NSNull null]])
        return @"";
    return str;
}
@end

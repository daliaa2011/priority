//
//  SideMenuModel.h
//  Priority
//
//  Created by AnAs EiD on 4/27/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "AppDataDelegate.h"
#import "SharedDataObject.h"

@interface SideMenuModel : NSObject{
    SharedDataObject *sharedDataObject;
}

- (NSMutableArray *)getOfferCategories;
-(void) insertOrUpdateCategory:(OfferCategoryList*) catgoryObj;
@end

//
//  SearchViewController.m
//  Priority
//
//  Created by AnAs EiD on 5/31/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "SearchViewController.h"
#import "SharedDataObject.h"

@interface SearchViewController (){
    NSMutableArray *offers;
    SharedDataObject *sharedDataObject;
}

@end

@implementation SearchViewController

- (SharedDataObject *)theAppDataObject{
    id<AppDataDelegate>delegate = (id<AppDataDelegate>)[UIApplication sharedApplication].delegate;
    SharedDataObject *dataObject;
    dataObject = (SharedDataObject *)delegate.theSharedDataObject;
    return dataObject;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    sharedDataObject = [self theAppDataObject];
    
    UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 30, 30)];
    [leftButton setImage:[UIImage imageNamed:@"Back"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    [[self navigationItem]setLeftBarButtonItem:leftBarButton];
    
    [_offersSearchBar becomeFirstResponder];
    
    
    self.searchedOffersTableView.backgroundColor = [UIColor clearColor];
    [self.searchedOffersTableView registerNib:[UINib nibWithNibName:@"OfferCustomTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"offerCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"offerCell";
    OfferCustomTableViewCell *offerCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    offerCell.backgroundColor = [UIColor clearColor];
    offerCell.contentView.backgroundColor = [UIColor clearColor];
    NSDictionary* offerDic = [offers objectAtIndex:indexPath.row];
    [[offerCell offerTitleLabel]setText:[offerDic objectForKey:@"offerName"]] ;
    offerCell.offerDetailsLabel.text = [offerDic objectForKey:@"offerDetails"];
    
    float distance = [[sharedDataObject currentLocation] distanceFromLocation:[offerDic objectForKey:@"businessLocation"]];
    [[offerCell distanceLabel]setText:[NSString stringWithFormat:@"%0.2f كم",distance/1000.0]];
    
    if ([offerDic objectForKey:@"offerLogo"]) {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = [offerCell offerImageView];
        [[offerCell offerImageView]sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/offers/%@",[offerDic objectForKey:@"offerLogo"]]]
                                                         andPlaceholderImage:[UIImage imageNamed:@"placeholders"]
                                                                     options:SDWebImageProgressiveDownload
                                                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                                            if (!activityIndicator) {
                                                                                [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                                [activityIndicator setCenter:[weakImageView center]];
                                                                                [activityIndicator startAnimating];
                                                                            }
                                                                        });
                                                                       
                                                                    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                                            [activityIndicator removeFromSuperview];
                                                                            activityIndicator = nil;
                                                                            if (error) {
                                                                                //NSLog(@"error : %@",error);
                                                                            }
                                                                        });
                                                                        
                                                                    }];
    }
    
    if ([offerDic objectForKey:@"businessLogo"]) {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = [offerCell logoImageView];
        [[offerCell logoImageView]sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/company/%@",[offerDic objectForKey:@"businessLogo"]]]
                                                        andPlaceholderImage:nil
                                                                    options:SDWebImageProgressiveDownload
                                                                   progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           if (!activityIndicator) {
                                                                               [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                               [activityIndicator setCenter:[weakImageView center]];
                                                                               [activityIndicator startAnimating];
                                                                           }
                                                                       });
                                                                       
                                                                   } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           [activityIndicator removeFromSuperview];
                                                                           activityIndicator = nil;
                                                                           if (error) {
                                                                               //NSLog(@"error : %@",error);
                                                                           }
                                                                       });
                                                                       
                                                                   }];
    }
    return offerCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [offers count];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat screenwidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = ((screenwidth - 20)/2 * (121.0/150)) + 10;
    return height;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    OfferDetailsViewController *OfferDetailsView = [[OfferDetailsViewController alloc]initWithNibName:@"OfferDetailsViewController" bundle:nil OfferID:[[[offers objectAtIndex:[indexPath row]]objectForKey:@"offerID"] intValue]];
    [[self navigationController]pushViewController:OfferDetailsView animated:YES];
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    //[exercisesSearchResult removeAllObjects];
    if (![searchText isEqual:@""]) {
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"تم" style:UIBarButtonItemStylePlain target:self action:@selector(doneSearching_Clicked:)];
        
        [rightBarButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Droid Arabic Kufi" size:16],NSFontAttributeName, nil] forState:UIControlStateNormal];
        [[self navigationItem]setRightBarButtonItem:rightBarButton];
    }
    else {
        self.navigationItem.rightBarButtonItem = nil;
    }
    [_layerButton setHidden:NO];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [_layerButton setHidden:NO];
    if ([offers count]) {
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"تم" style:UIBarButtonItemStylePlain target:self action:@selector(doneSearching_Clicked:)];
        
        [rightBarButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Droid Arabic Kufi" size:16],NSFontAttributeName, nil] forState:UIControlStateNormal];
        [[self navigationItem]setRightBarButtonItem:rightBarButton];
    }
    else {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self searchTableView:[searchBar text]];
}

#pragma mark - UserDefiendFunctions
- (void)popViewController{
    [[self navigationController]popViewControllerAnimated:YES];
}

- (void)searchTableView:(NSString *)keyword{
    //send Search Request on button click
    [ServerAPI SearchOffersByKeyword:keyword Success:^(id response) {
        offers = [NSMutableArray array];
        for (NSMutableArray *rawOffer in response) {
            [offers addObject:[[OfferObject alloc] createArrayObjectWithResponce:rawOffer]];
        }
        [_layerButton setHidden:YES];
        [_offersSearchBar resignFirstResponder];
        self.navigationItem.rightBarButtonItem = nil;
        [_searchedOffersTableView reloadData];
    } Failure:^(NSString *errorString) {
        NSLog(@"Error : %@",errorString);
    }];
    
}

- (void)doneSearching_Clicked:(id)sender{
    if ([offers count]) {
        [_layerButton setHidden:YES];
    }
    [_offersSearchBar resignFirstResponder];
    self.navigationItem.rightBarButtonItem = nil;
}

@end

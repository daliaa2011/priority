//
//  OfferVCodeViewController.h
//  Priority
//
//  Created by AnAs EiD on 5/27/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "SharedDataObject.h"
#import "AppDataDelegate.h"

@interface OfferVCodeViewController : UIViewController{
    SharedDataObject *sharedDataObject;
    
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil Offer:(NSMutableDictionary *)offer;

@property (weak, nonatomic) IBOutlet UILabel *offerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *vCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *expierationLabel;
@end

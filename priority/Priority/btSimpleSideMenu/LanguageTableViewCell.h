//
//  LanguageTableViewCell.h
//  Priority
//
//  Created by Dalia Mahmoud on 7/10/17.
//  Copyright © 2017 Arab Mobile Content. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UISwitch *arSwitch;
@property (nonatomic,weak) IBOutlet UILabel* cellLabel;
@end

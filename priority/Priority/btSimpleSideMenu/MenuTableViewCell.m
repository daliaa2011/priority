//
//  MenuTableViewCell.m
//  News
//
//  Created by Dalia Abd El-Hadi on 3/2/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.cellLabel.textColor = [UIColor whiteColor];
    self.iconImageView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.cellButton.hidden = YES;
    self.delegate = nil;
}

- (IBAction)buttonClicked
{
    [self.delegate loadSource:self.catObject];
}
@end

//
//  MoreTableViewCell.m
//  Priority
//
//  Created by Dalia Mahmoud on 7/10/17.
//  Copyright © 2017 Arab Mobile Content. All rights reserved.
//

#import "MoreTableViewCell.h"

@implementation MoreTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

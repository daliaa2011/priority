//
//  MoreTableViewCell.h
//  Priority
//
//  Created by Dalia Mahmoud on 7/10/17.
//  Copyright © 2017 Arab Mobile Content. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel* cellLabel;
@property (nonatomic,weak) IBOutlet UIImageView *arrowImageView;
@end

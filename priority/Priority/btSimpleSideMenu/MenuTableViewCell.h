//
//  MenuTableViewCell.h
//  News
//
//  Created by Dalia Abd El-Hadi on 3/2/15.
//  Copyright (c) 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuCellDelegate <NSObject>

- (void) loadSource:(id) source;

@end

@interface MenuTableViewCell : UITableViewCell

@property (nonatomic,strong) id catObject;
@property (nonatomic,weak) IBOutlet UIImageView* iconImageView;
@property (nonatomic,weak) IBOutlet UILabel* cellLabel;
@property (nonatomic,weak) IBOutlet UIButton* cellButton;
@property (nonatomic,weak) id <MenuCellDelegate> delegate;
- (IBAction)buttonClicked;
@end

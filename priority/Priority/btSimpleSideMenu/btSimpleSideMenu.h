//
//  BTSimpleSideMenu.h
//  BTSimpleSideMenuDemo
//
//  Created by Balram on 29/05/14.
//  Copyright (c) 2014 Balram Tiwari. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MenuTableViewCell.h"
#import "SharedDataObject.h"

#import "SideMenuModel.h"
#import "CategoryOffersViewController.h"
#import "MyPriorityViewController.h"
#import "FAQViewController.h"
#import "TermsViewController.h"

@class BTSimpleSideMenu;

@protocol BTSimpleSideMenuDelegate <NSObject>

- (void) goToHome;
- (void) goToMySection;
- (void) goToFAQ;
- (void) goToTerms;
- (void) goToCategory:(NSInteger) categoryID;
@end

@interface BTSimpleSideMenu : UIView<UITableViewDelegate, UITableViewDataSource, MenuCellDelegate> {
    @private
    UITableView *menuTable;
    CGFloat yAxis,height, width;

    BOOL isOpen;
    UITapGestureRecognizer *gesture;
    UISwipeGestureRecognizer *leftSwipe, *rightSwipe;
    
    
    SharedDataObject *sharedDataObject;
    SideMenuModel *sideMenuModel;
    
    NSMutableArray *offersCategories;
    
    BOOL moreIsExpanded;
}

@property(nonatomic, weak) id <BTSimpleSideMenuDelegate> delegate;
@property (nonatomic,assign) BOOL isOpen;
-(instancetype) initAndaddToViewController:(id)sender;
-(void)show;
-(void)hide;
-(void)toggleMenu;

- (void) setupView;
@end

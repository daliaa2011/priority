//
//  BTSimpleSideMenu.m
//  BTSimpleSideMenuDemo
//  Created by Balram on 29/05/14.
//  Copyright (c) 2014 Balram Tiwari. All rights reserved.
//

#define BACKGROUND_COLOR [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.9]

#import "BTSimpleSideMenu.h"
#import <QuartzCore/QuartzCore.h>
#import <Accelerate/Accelerate.h>
#import "MoreTableViewCell.h"
#import "LanguageTableViewCell.h"
#import "Priority-Swift.h"
#import "MainScreenViewController.h"
#import "AppDelegate.h"

@implementation BTSimpleSideMenu
@synthesize isOpen;
#pragma -mark public methods

- (SharedDataObject *)theAppDataObject{
    id<AppDataDelegate>delegate = (id<AppDataDelegate>)[UIApplication sharedApplication].delegate;
    SharedDataObject *dataObject;
    dataObject = (SharedDataObject *)delegate.theSharedDataObject;
    return dataObject;
}

-(instancetype) initAndaddToViewController:(id)sender {
    if ((self = [super init])) {
        // perform the other initialization of items.
        [self commonInit:sender];
    }
    return self;
}


- (void) setupView
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMenu) name:@"RefreshMenuNotification" object:nil];
    
    yAxis = 0;
    self.backgroundColor = BACKGROUND_COLOR;
    
    menuTable = [[UITableView alloc]initWithFrame:self.bounds style:UITableViewStyleGrouped];
    menuTable.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [menuTable registerNib:[UINib nibWithNibName:@"MenuTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"MenuCell"];
    [menuTable registerNib:[UINib nibWithNibName:@"MoreTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"MoreCell"];
    [menuTable registerNib:[UINib nibWithNibName:@"LanguageTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LanguageTableViewCell"];
    
    [menuTable registerNib:[UINib nibWithNibName:@"MenuButtonsTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"MenuButtonsTableViewCellIdentifier"];
    
    [menuTable setBackgroundColor:[UIColor clearColor]];
    [menuTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [menuTable setShowsVerticalScrollIndicator:NO];
    
    menuTable.delegate = self;
    menuTable.dataSource = self;
    //menuTable.alpha = 0;
    isOpen = NO;
    [self addSubview:menuTable];
}
-(void)toggleMenu{
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        if(!isOpen){
            [self show];
        }else {
            [self hide];
        }
    }
   
}

-(void)show{
    if(!isOpen){
        [self localizeSubViews];
        offersCategories = [[NSMutableArray alloc]initWithArray:[sideMenuModel getOfferCategories]];
        
        [menuTable reloadData];
        [UIView animateWithDuration:0.5 animations:^{
            BOOL englishLang = [[NSUserDefaults standardUserDefaults] boolForKey:@"englishLang"];
            CGRect screenSize = [UIScreen mainScreen].bounds;
            if (englishLang)
            {
                self.frame = CGRectMake(0, yAxis, width, screenSize.size.height);
            }
            else {
                self.frame = CGRectMake(screenSize.size.width - width, yAxis, width, screenSize.size.height);
            }
            
            menuTable.alpha = 1;
        } completion:^(BOOL finished) {
            
        }];
        isOpen = YES;
    }
}

-(void)hide {
    if(isOpen){
        [UIView animateWithDuration:0.5 animations:^{
            CGRect screenSize = [UIScreen mainScreen].bounds;
            BOOL englishLang = [[NSUserDefaults standardUserDefaults] boolForKey:@"englishLang"];
            if (englishLang){
                self.frame = CGRectMake(-1 * width, yAxis, width, screenSize.size.height);
            } else {
                self.frame = CGRectMake(screenSize.size.width, yAxis, width, screenSize.size.height);
            }
            
            menuTable.alpha = 0;
        }];
        isOpen = NO;
    }
}

#pragma -mark tableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (moreIsExpanded) {
        return 2 + offersCategories.count + 4;
    }
    else {
        return 2 + offersCategories.count;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return 46;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0)
    {
        [self.delegate goToHome];
    }
    else if (indexPath.row == offersCategories.count + 1) {
        moreIsExpanded = !moreIsExpanded;
        [tableView reloadData];
    }
    else if (indexPath.row == offersCategories.count + 2) {
        [self.delegate goToMySection];
    }
    else if (indexPath.row <= offersCategories.count )
    {
        [self.delegate goToCategory:[[offersCategories objectAtIndex:indexPath.row - 1] _ID]];
    }
    else if (indexPath.row == offersCategories.count + 3) {
        //FAQ
        [self.delegate goToFAQ];
    }
    else if (indexPath.row == offersCategories.count + 4) {
        //Terms
        [self.delegate goToTerms];
    }
}

#define MAIN_VIEW_TAG 1
#define TITLE_LABLE_TAG 2
#define IMAGE_VIEW_TAG 3

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     BOOL englishLang = [[NSUserDefaults standardUserDefaults] boolForKey:@"englishLang"];
    if (indexPath.row == offersCategories.count + 1) {
        NSString *identifier = @"MoreCell";
        MoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil){
            cell = [[MoreTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (englishLang) {
            cell.cellLabel.text = @"More";
        }
        else {
            cell.cellLabel.text = @"المزيد";
        }
        if (moreIsExpanded) {
            cell.arrowImageView.image = [UIImage imageNamed:@"arrow-up"];
        }
        else {
             cell.arrowImageView.image = [UIImage imageNamed:@"arrow-down"];
        }
        return cell;
    }
    else if (indexPath.row == offersCategories.count + 5) {
        NSString *identifier = @"LanguageTableViewCell";
        LanguageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil){
            cell = [[LanguageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        [cell.arSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
        if (englishLang) {
            cell.cellLabel.text = @"Language";
            [cell.arSwitch setOn:NO];
        }
        else {
            cell.cellLabel.text = @"اللغة";
            [cell.arSwitch setOn:YES];
        }
        return cell;
    }
    else {
        NSString *identifier = @"MenuCell";
        MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil){
            cell = [[MenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.cellLabel.text = @"";
        cell.iconImageView.image = nil;
        if (indexPath.row == 0)
        {
            if (englishLang) {
                cell.cellLabel.text = @"Home";
            }
            else {
                cell.cellLabel.text = @"الرئيسية";
            }
            
            cell.iconImageView.image = [UIImage imageNamed:@"home"];
        }
        else if (indexPath.row <= offersCategories.count )
        {
            if (englishLang){
                [cell.cellLabel setText:[[offersCategories objectAtIndex:indexPath.row - 1] NameEn]];
            }
            else {
                [cell.cellLabel setText:[[offersCategories objectAtIndex:indexPath.row - 1] name]];
            }
            
            [cell.iconImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]];
        }
        else if (indexPath.row == offersCategories.count + 2) {
            //
            if (englishLang) {
                cell.cellLabel.text = @"My area";
            }
            else {
                cell.cellLabel.text = @"أنا مميز";
            }
        }
        else if (indexPath.row == offersCategories.count + 3) {
            //
            if (englishLang) {
                cell.cellLabel.text = @"FAQ";
            }
            else {
                cell.cellLabel.text = @"أسئلة متكررة";
            }
        }
        else if (indexPath.row == offersCategories.count + 4) {
            //
            if (englishLang) {
                cell.cellLabel.text = @"Terms and conditions";
            }
            else {
                cell.cellLabel.text = @"الشروط والأحكام";
            }
        }
        return cell;
    }
}

-(void) switchValueChanged:(id) sender {
    UISwitch *langSwitch = sender;
    if (langSwitch.isOn) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"englishLang"];
        [L102Language setAppleLAnguageToLang:@"ar"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        [[UITextField appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"englishLang"];
        [L102Language setAppleLAnguageToLang:@"en"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        [[UITextField appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
    }
    
    MainScreenViewController* _mainScreenView = [[MainScreenViewController alloc]init];
    
    UINavigationController* _navigationController = [[UINavigationController alloc]initWithRootViewController:_mainScreenView];
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).theSharedDataObject.sideMenu = [[BTSimpleSideMenu alloc] initAndaddToViewController:_navigationController];
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).theSharedDataObject.sideMenu.delegate = _mainScreenView;
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).window.rootViewController = _navigationController;
    [((AppDelegate*)[[UIApplication sharedApplication] delegate]).window makeKeyAndVisible];

}
#pragma -mark Private helpers
-(void)commonInit:(UIViewController *)sender{
    
    sharedDataObject = [self theAppDataObject];
    sideMenuModel = [[SideMenuModel alloc]init];
    offersCategories = [[NSMutableArray alloc]initWithArray:[sideMenuModel getOfferCategories]];
    
    CGRect screenSize = [UIScreen mainScreen].bounds;
    
    yAxis = 0;
    height = screenSize.size.height;
    width = screenSize.size.width;

    
    BOOL englishLang = [[NSUserDefaults standardUserDefaults] boolForKey:@"englishLang"];
    if (englishLang){
        self.frame = CGRectMake(-1 * width, yAxis, width, height);
    } else {
        self.frame = CGRectMake(screenSize.size.width, yAxis, width, height);
    }
    
    self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin;
    self.backgroundColor = BACKGROUND_COLOR;
    
    UINavigationBar* navbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, width, ((UINavigationController*)sender).navigationBar.frame.size.height + 20)];
    [navbar setTranslucent:NO];
    [navbar setBarTintColor:[UIColor colorWithRed:235.0/255.0 green:86.0/255.0 blue:62.0/255.0 alpha:1]];
    [self addSubview:navbar];
    
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 0.0, 150, 44.0)];
    [titleLabel setText:NSLocalizedString(@"menu", @"menu")];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    
    titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi" size:16.0];;
    [navItem setTitleView:titleLabel];
    
    UIButton* closeMenuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [closeMenuButton setImage:[UIImage imageNamed:@"closeMenu"] forState:UIControlStateNormal];
    [closeMenuButton addTarget:self action:@selector(toggleMenu) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:closeMenuButton];
    [navItem setRightBarButtonItem:rightBarButton animated:YES];
    
    [navbar setItems:[NSArray arrayWithObject:navItem] animated:NO];
    
    UIView* topSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, navbar.frame.size.height, width, 3)];
    topSeparator.backgroundColor = UIColorFromRGB(0xf1583f);
    [self addSubview:topSeparator];
    
    menuTable = [[UITableView alloc]initWithFrame:CGRectMake(0, topSeparator.frame.origin.y + topSeparator.frame.size.height, width, height - navbar.frame.size.height - topSeparator.frame.size.height ) style:UITableViewStyleGrouped];
    menuTable.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [menuTable registerNib:[UINib nibWithNibName:@"MenuTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"MenuCell"];
    [menuTable registerNib:[UINib nibWithNibName:@"MoreTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"MoreCell"];
    [menuTable registerNib:[UINib nibWithNibName:@"LanguageTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LanguageTableViewCell"];
    
    [menuTable setBackgroundColor:[UIColor clearColor]];
    [menuTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [menuTable setShowsVerticalScrollIndicator:NO];
//    menuTable.contentInset = UIEdgeInsetsZero;
    menuTable.delegate = self;
    menuTable.dataSource = self;
    menuTable.alpha = 0;
    isOpen = NO;
    [self addSubview:menuTable];
    
    gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(toggleMenu)];
    gesture.numberOfTapsRequired = 2;
    
    leftSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(show)];
    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    
    rightSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
    rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    
    [sender.view addSubview:self];
     [sender.view addGestureRecognizer:rightSwipe];
    [sender.view addGestureRecognizer:leftSwipe];
    moreIsExpanded = NO;
}
@end


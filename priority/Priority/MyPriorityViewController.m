//
//  MyPriorityViewController.m
//  Priority
//
//  Created by AnAs EiD on 5/3/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "MyPriorityViewController.h"
#import "SearchViewController.h"
#import "btSimpleSideMenu.h"
#import "Priority-Swift.h"

@interface MyPriorityViewController (){
    NSMutableArray *offers;
}
@end

@implementation MyPriorityViewController

- (SharedDataObject *)theAppDataObject{
    id<AppDataDelegate>delegate = (id<AppDataDelegate>)[UIApplication sharedApplication].delegate;
    SharedDataObject *dataObject;
    dataObject = (SharedDataObject *)delegate.theSharedDataObject;
    return dataObject;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        sharedDataObject = [self theAppDataObject];
        [[sharedDataObject _DBObject]connectDB];
        //get All Offers (Saved & Used)
        offers = [[sharedDataObject _DBObject]getOffers:"SELECT BusinessTBL.Address,BusinessTBL.CategoryID,BusinessTBL.City,BusinessTBL.CountryID,BusinessTBL.Details,BusinessTBL.ID,BusinessTBL.Latitude,BusinessTBL.Logo,BusinessTBL.Longitude,BusinessTBL.Name,OfferTBL.CodeType,OfferTBL.Details,OfferTBL.EndDate,OfferTBL.EndTime,OfferTBL.ID,OfferTBL.Logo,OfferTBL.Name,OfferTBL.StartDate,OfferTBL.StartTime,OfferTBL.VCode FROM BusinessTBL INNER JOIN  OfferTBL ON OfferTBL.BussinessesTBLID = BusinessTBL.ID"];
        [[sharedDataObject _DBObject]disConnectDB];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.myOffersTableView.backgroundColor = [UIColor clearColor];
    [self.myOffersTableView registerNib:[UINib nibWithNibName:@"OfferCustomTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"offerCell"];
    
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    titleLabel.text = NSLocalizedString(@"priority", @"priority");
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi" size:16.0];
    titleLabel.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabel;
    
    
    UIButton *rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 20, 20)];
    [rightButton setImage:[UIImage imageNamed:@"Menu"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    [[self navigationItem]setRightBarButtonItem:rightBarButton];
    
    UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 20, 20)];
    [leftButton setImage:[UIImage imageNamed:@"Search"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(searchOffers) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    [[self navigationItem]setLeftBarButtonItem:leftBarButton];
    
    
//    UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 30, 30)];
//    [leftButton setImage:[UIImage imageNamed:@"Back"] forState:UIControlStateNormal];
//    [leftButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
//    [[self navigationItem]setLeftBarButtonItem:leftBarButton];
    
    if ([_myOffersTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_myOffersTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view localizeSubViews];

}
#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"offerCell";
    OfferCustomTableViewCell *offerCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    offerCell.backgroundColor = [UIColor clearColor];
    offerCell.contentView.backgroundColor = [UIColor clearColor];
    NSDictionary* offerDic = [offers objectAtIndex:indexPath.row];
    [[offerCell offerTitleLabel]setText:[offerDic objectForKey:@"offerName"]] ;
    offerCell.offerDetailsLabel.text = [offerDic objectForKey:@"offerDetails"];
//    [[offerCell distanceLabel]setText:[NSString stringWithFormat:@"%@",[offerDic objectForKey:@"businessDistance"]]];
    
//    [offerDic setObject:[NSNumber numberWithInt:[[sharedDataObject currentLocation] distanceFromLocation:[offerDic objectForKey:@"businessLocation"]]] forKey:@"businessDistance"];
    float distance = [[sharedDataObject currentLocation] distanceFromLocation:[offerDic objectForKey:@"businessLocation"]];
    [[offerCell distanceLabel]setText:[NSString stringWithFormat:@"%0.2f كم",distance/1000.0]];
    
    
//    offerCell.businessNameLabel.text = [offerDic objectForKey:@"businessName"];
    
    if ([offerDic objectForKey:@"offerLogo"]) {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = [offerCell offerImageView];
        [[offerCell offerImageView]sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/offers/%@",[offerDic objectForKey:@"offerLogo"]]]
                                                         andPlaceholderImage:[UIImage imageNamed:@"placeholders"]
                                                                     options:SDWebImageProgressiveDownload
                                                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                                            if (!activityIndicator) {
                                                                                [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                                [activityIndicator setCenter:[weakImageView center]];
                                                                                [activityIndicator startAnimating];
                                                                            }
                                                                        });
                                                                        
                                                                    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                                            [activityIndicator removeFromSuperview];
                                                                            activityIndicator = nil;
                                                                            if (error) {
                                                                                //NSLog(@"error : %@",error);
                                                                            }
                                                                        });
                                                                        
                                                                    }];
    }
    
    if ([offerDic objectForKey:@"businessLogo"]) {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = [offerCell logoImageView];
        [[offerCell logoImageView]sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://priooffers.blob.core.windows.net/company/%@",[offerDic objectForKey:@"businessLogo"]]]
                                                        andPlaceholderImage:nil
                                                                    options:SDWebImageProgressiveDownload
                                                                   progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           if (!activityIndicator) {
                                                                               [weakImageView addSubview:activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                               [activityIndicator setCenter:[weakImageView center]];
                                                                               [activityIndicator startAnimating];
                                                                           }
                                                                       });
                                                                       
                                                                   } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           [activityIndicator removeFromSuperview];
                                                                           activityIndicator = nil;
                                                                           if (error) {
                                                                               //NSLog(@"error : %@",error);
                                                                           }
                                                                       });
                                                                       
                                                                   }];
    }
    return offerCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [offers count];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat screenwidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = ((screenwidth - 20)/2 * (121.0/150)) + 10;
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    OfferDetailsViewController *OfferDetailsView = [[OfferDetailsViewController alloc]initWithNibName:@"OfferDetailsViewController" bundle:nil OfferID:[[[offers objectAtIndex:[indexPath row]]objectForKey:@"offerID"] intValue]];
    [[self navigationController]pushViewController:OfferDetailsView animated:YES];
}

#pragma mark - UserDefienedFunctions
- (void)popViewController{
    [[self navigationController]popViewControllerAnimated:YES];
}

- (IBAction)showSavedOffers:(id)sender{
    [_ind1 setHidden:NO];
    [_ind2 setHidden:YES];
    [_ind3 setHidden:YES];
    [[sharedDataObject _DBObject]connectDB];
    offers = [[sharedDataObject _DBObject]getOffers:"SELECT BusinessTBL.Address,BusinessTBL.CategoryID,BusinessTBL.City,BusinessTBL.CountryID,BusinessTBL.Details,BusinessTBL.ID,BusinessTBL.Latitude,BusinessTBL.Logo,BusinessTBL.Longitude,BusinessTBL.Name,OfferTBL.CodeType,OfferTBL.Details,OfferTBL.EndDate,OfferTBL.EndTime,OfferTBL.ID,OfferTBL.Logo,OfferTBL.Name,OfferTBL.StartDate,OfferTBL.StartTime,OfferTBL.VCode FROM BusinessTBL INNER JOIN  OfferTBL ON OfferTBL.BussinessesTBLID = BusinessTBL.ID WHERE OfferTBL.VCode = '-'"];
    [[sharedDataObject _DBObject]disConnectDB];
    [_myOffersTableView reloadData];
}

- (IBAction)showUsedOffers:(id)sender{
    [_ind1 setHidden:YES];
    [_ind2 setHidden:NO];
    [_ind3 setHidden:YES];
    [[sharedDataObject _DBObject]connectDB];
    offers = [[sharedDataObject _DBObject]getOffers:"SELECT BusinessTBL.Address,BusinessTBL.CategoryID,BusinessTBL.City,BusinessTBL.CountryID,BusinessTBL.Details,BusinessTBL.ID,BusinessTBL.Latitude,BusinessTBL.Logo,BusinessTBL.Longitude,BusinessTBL.Name,OfferTBL.CodeType,OfferTBL.Details,OfferTBL.EndDate,OfferTBL.EndTime,OfferTBL.ID,OfferTBL.Logo,OfferTBL.Name,OfferTBL.StartDate,OfferTBL.StartTime,OfferTBL.VCode FROM BusinessTBL INNER JOIN  OfferTBL ON OfferTBL.BussinessesTBLID = BusinessTBL.ID WHERE OfferTBL.VCode <> '-'"];
    [[sharedDataObject _DBObject]disConnectDB];
    [_myOffersTableView reloadData];
}

- (IBAction)showAllOffers:(id)sender{
    [_ind1 setHidden:YES];
    [_ind2 setHidden:YES];
    [_ind3 setHidden:NO];
    [[sharedDataObject _DBObject]connectDB];
    offers = [[sharedDataObject _DBObject]getOffers:"SELECT BusinessTBL.Address,BusinessTBL.CategoryID,BusinessTBL.City,BusinessTBL.CountryID,BusinessTBL.Details,BusinessTBL.ID,BusinessTBL.Latitude,BusinessTBL.Logo,BusinessTBL.Longitude,BusinessTBL.Name,OfferTBL.CodeType,OfferTBL.Details,OfferTBL.EndDate,OfferTBL.EndTime,OfferTBL.ID,OfferTBL.Logo,OfferTBL.Name,OfferTBL.StartDate,OfferTBL.StartTime,OfferTBL.VCode FROM BusinessTBL INNER JOIN  OfferTBL ON OfferTBL.BussinessesTBLID = BusinessTBL.ID"];
    [[sharedDataObject _DBObject]disConnectDB];
    [_myOffersTableView reloadData];
}

#pragma mark - UserDefiendFunctions
- (void)rightSideMenuButtonPressed {
    //[[self menuContainerViewController]toggleRightSideMenuCompletion:^{}];
    [sharedDataObject.sideMenu toggleMenu];
}

- (void)searchOffers {
    SearchViewController *searchView = [[SearchViewController alloc]initWithNibName:@"SearchViewController" bundle:nil];
    [[self navigationController]pushViewController:searchView animated:YES];
}

@end

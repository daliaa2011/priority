//
//  MainScreenViewController.h
//  Priority
//
//  Created by AnAs EiD on 4/13/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//



#import "AppDataDelegate.h"

#import "MainScreenModel.h"
#import "ActivationViewController.h"
#import "SearchViewController.h"
#import "FeaturedOfferCustomTableViewCell.h"
#import "OfferDetailsViewController.h"

#import "btSimpleSideMenu.h"

@interface MainScreenViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate , BTSimpleSideMenuDelegate>{
    SharedDataObject *sharedDataObject;
    MainScreenModel *mainScreenModel;
}

@property (weak, nonatomic) IBOutlet UITableView *featuredOffersTableView;
@end

//
//  OfferVCodeViewController.m
//  Priority
//
//  Created by AnAs EiD on 5/27/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "OfferVCodeViewController.h"
#import "Priority-Swift.h"

@interface OfferVCodeViewController (){
    NSMutableDictionary *_offer;
    NSDateComponents *components;
}
@end

@implementation OfferVCodeViewController

- (SharedDataObject *)theAppDataObject{
    id<AppDataDelegate>delegate = (id<AppDataDelegate>)[UIApplication sharedApplication].delegate;
    SharedDataObject *dataObject;
    dataObject = (SharedDataObject *)delegate.theSharedDataObject;
    return dataObject;
}


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil Offer:(NSMutableDictionary *)offer{
    self = [super init];
    if (self) {
        sharedDataObject = [self theAppDataObject];
        _offer = offer;
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view localizeSubViews];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initUI];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UserDefienedFunctions
- (void)popViewController{
    [[self navigationController]popViewControllerAnimated:YES];
}

- (void)initUI{
    
    UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 30, 30)];
    [leftButton setImage:[UIImage imageNamed:@"Back"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    [[self navigationItem]setLeftBarButtonItem:leftBarButton];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    titleLabel.text = NSLocalizedString(@"priority", @"priority");
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi" size:16.0];
    titleLabel.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabel;
    
    self.vCodeLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    self.vCodeLabel.layer.borderWidth = 1.0;
    
    
    components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:[NSDate date] toDate:[_offer objectForKey:@"offerEndDate"] options:0];
    
    [_offerTitleLabel setText:[_offer objectForKey:@"offerName"]];
    
    if ([[_offer objectForKey:@"offerVCode"]isEqualToString:@"Discontinued"]) {
         [_vCodeLabel setText:@"لا يوجد رمز"];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"نأسف" message:@"لقد إنتهى العرض" delegate:nil cancelButtonTitle:@"إخفاء" otherButtonTitles:nil, nil];
        [alert show];
    }
    else {
        switch ([[_offer objectForKey:@"offerCodeType"] intValue]) {
            case 1:
            {
                [_vCodeLabel setText:@"لا يوجد رمز"];
            }
                break;
            case 2:
            {
                [_vCodeLabel setText:[_offer objectForKey:@"offerVCode"]];
            }
                break;
            case 3:
            {
                [_vCodeLabel setText:[_offer objectForKey:@"offerVCode"]];
            }
                break;
            default:
                break;
        }
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *timestamp = [dateFormatter stringFromDate:[_offer objectForKey:@"offerEndDate"]];
    
    [_expierationLabel setText:[NSString stringWithFormat:@"تاريخ الانتهاء : %@",timestamp]];
}


@end

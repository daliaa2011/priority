//
//  ActivationViewController.h
//  Priority
//
//  Created by AnAs EiD on 4/21/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivationViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *stepsNumberLabel;
@property (weak, nonatomic) IBOutlet UITextField *inputValueTextField;
@property (weak, nonatomic) IBOutlet UIButton *changePhoneNumButton;
@property (weak, nonatomic) IBOutlet UIButton *resendCodeButton;
@property (weak, nonatomic) IBOutlet UILabel *condition1Label;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UILabel *condition2Label;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputViewLayoutConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainImageLayoutConstraint;

- (IBAction)sendPhoneNumber:(id)sender;
- (IBAction)changePhoneNumber:(id)sender;

@end

//
//  CategoryOffersViewController.h
//  Priority
//
//  Created by AnAs EiD on 4/29/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "AppDataDelegate.h"
#import "SharedDataObject.h"

#import "OfferCustomTableViewCell.h"
#import "OfferDetailsViewController.h"
#import "DTParallaxTableView.h"
#import "DTParallaxHeaderView.h"

@interface CategoryOffersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>{
    SharedDataObject *sharedDataObject;
    
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil CategoryID:(NSInteger)categoryID;

@property (weak, nonatomic) IBOutlet UIImageView *categoryCoverImageView;

@property (weak, nonatomic) IBOutlet UIButton *nearestButton;
@property (weak, nonatomic) IBOutlet UIButton *newestButton;

@property (retain, nonatomic) IBOutlet DTParallaxTableView *categoryOffersTableView;
@property (retain, nonatomic) IBOutlet DTParallaxHeaderView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (strong, nonatomic) IBOutlet UIView* buttonsView;
//@property (strong, nonatomic) IBOutlet UIView* headerView;

- (IBAction)showNearestOffers:(id)sender;
- (IBAction)showNewestOffers:(id)sender;

@end

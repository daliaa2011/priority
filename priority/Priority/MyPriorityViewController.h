//
//  MyPriorityViewController.h
//  Priority
//
//  Created by AnAs EiD on 5/3/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "SharedDataObject.h"
#import "AppDataDelegate.h"

#import "OfferCustomTableViewCell.h"
#import "OfferDetailsViewController.h"

@interface MyPriorityViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    SharedDataObject *sharedDataObject;
}

@property (retain, nonatomic) IBOutlet UITableView *myOffersTableView;
@property (weak, nonatomic) IBOutlet UILabel *ind1;
@property (weak, nonatomic) IBOutlet UILabel *ind2;
@property (weak, nonatomic) IBOutlet UILabel *ind3;

- (IBAction)showSavedOffers:(id)sender;
- (IBAction)showUsedOffers:(id)sender;
- (IBAction)showAllOffers:(id)sender;
@end

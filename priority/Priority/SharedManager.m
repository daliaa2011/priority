//
//  SharedManager.m
//  IslamAudio
//
//  Created by Dalia Abd El-Hadi on 10/21/15.
//  Copyright © 2015 Dalia Abd El-Hadi. All rights reserved.
//

#import "SharedManager.h"

@implementation SharedManager

+ (SharedManager *) sharedInstance {
    static SharedManager *_sharedObject = nil;
    static dispatch_once_t onceToken;
    dispatch_once (&onceToken, ^{
        _sharedObject = [SharedManager new];
    });
    return _sharedObject;
}

- (id) init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (BOOL) validateEmail:(NSString*)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (NSString*) getStringValue:(NSString*) str
{
    if (!str || [str isEqual:[NSNull null]])
        return @"";
    return str;
}

@end

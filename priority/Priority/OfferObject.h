//
//  OfferObject.h
//  Priority
//
//  Created by AnAs EiD on 5/20/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SharedDataObject.h"
#import "AppDataDelegate.h"

@interface OfferObject : NSObject{
}

- (instancetype)initWithResponce:(id)response;
- (NSMutableDictionary *)createArrayObjectWithResponce:(id)response;
- (NSString *)getOfferCategoryName:(int)typeID;

//Offer Values (Business & offer paramaters)
@property (nonatomic, retain) NSString *businessAddress;
@property int CatsTBLID;
@property (nonatomic, retain) NSString *businessCity;
@property int CountryId;
@property (nonatomic, retain) NSString *businessDetails;
@property int businessDistance;
@property (nonatomic, retain) NSString *businessID;
@property (nonatomic, retain) CLLocation *businessLocation;
@property (nonatomic, retain) NSString *businessName;
@property (nonatomic, retain) NSString *offerDetails;
@property (nonatomic, retain) NSDate   *offerEndDate;
@property (nonatomic, retain) NSDate   *offerEndTime;
@property (nonatomic, retain) NSNumber *offerGetFree;
@property (nonatomic, retain) NSString *offerName;
@property (nonatomic, retain) NSNumber *offerNum;
@property (nonatomic, retain) NSDate   *offerStartDate;
@property (nonatomic, retain) NSDate   *offerStartTime;
@property (nonatomic, retain) NSNumber *offerType;
@property (nonatomic, retain) NSString *offerVCode;
@property (retain, nonatomic) NSString *title;
@property (retain, nonatomic) NSString *OfferDesc;


@end

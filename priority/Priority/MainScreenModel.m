//
//  MainScreenModel.m
//  Priority
//
//  Created by AnAs EiD on 5/10/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "MainScreenModel.h"

@implementation MainScreenModel

- (SharedDataObject *)theAppDataObject{
    id<AppDataDelegate>delegate = (id<AppDataDelegate>)[UIApplication sharedApplication].delegate;
    SharedDataObject *dataObject;
    dataObject = (SharedDataObject *)delegate.theSharedDataObject;
    return dataObject;
}

- (NSString *)getCategoryNameForID:(int)categoryID{
    NSString *categoryName;
    SharedDataObject *sharedDataObject = [self theAppDataObject];
    NSString *query = @"";
    BOOL englishLang = [[NSUserDefaults standardUserDefaults] boolForKey:@"englishLang"];
    if (englishLang) {
        query = [[NSString alloc]initWithFormat:@"SELECT NameEn FROM OfferCategoryTBL WHERE ID = %d",categoryID];
    }
    else {
        query = [[NSString alloc]initWithFormat:@"SELECT Name FROM OfferCategoryTBL WHERE ID = %d",categoryID];
    }
    
    [[sharedDataObject _DBObject]connectDB];
    categoryName = [[sharedDataObject _DBObject]getDataText:[query UTF8String]];
    [[sharedDataObject _DBObject]disConnectDB];
    return categoryName;
}

@end

//
//  SideMenuViewController.h
//  Priority
//
//  Created by AnAs EiD on 4/13/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "AppDataDelegate.h"
#import "SharedDataObject.h"

#import "SideMenuModel.h"
#import "CategoryOffersViewController.h"
#import "MyPriorityViewController.h"

@interface SideMenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    SharedDataObject *sharedDataObject;
    SideMenuModel *sideMenuModel;
    
    NSMutableArray *offersCategories;
}

@property (strong, nonatomic) UINavigationController *navigationController;
@property(nonatomic,weak) IBOutlet UITableView *tableView;
@end

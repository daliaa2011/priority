//
//  SharedDataObject.h
//  Priority
//
//  Created by AnAs EiD on 4/12/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>

#import "AppDataObject.h"
#import "PriorityDAO.h"
#import "ServerAPI.h"
#import "OfferObject.h"

#import "MFSideMenu.h"
#import "MBProgressHUD.h"
#import "HMSegmentedControl.h"
#import "SDWebImage/SDImageCache.h"
#import "SDWebImage/UIImageView+WebCache.h"

@class BTSimpleSideMenu;

#pragma mark - MACROS :
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#define priority_SERVER_ADDRESS https://

#define DATE_COMPONENTS (NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)
#define CURRENT_CALENDAR [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar]

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(_SCREEN_WIDTH, _SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(_SCREEN_WIDTH, _SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


@interface SharedDataObject : AppDataObject

@property PriorityDAO *_DBObject;

@property (retain, nonatomic) MBProgressHUD *mainHUD;
@property (retain, nonatomic) CLLocationManager *locationManager;
@property (retain, nonatomic) CLLocation *currentLocation;

@property (nonatomic,strong) BTSimpleSideMenu *sideMenu;

-(void) loadCategories;
@end

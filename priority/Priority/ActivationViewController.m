//
//  ActivationViewController.m
//  Priority
//
//  Created by AnAs EiD on 4/21/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "ActivationViewController.h"

@interface ActivationViewController ()

@end

@implementation ActivationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)moveInputView:(id)sender {
    [_inputViewLayoutConstraint setConstant:215.0];
    [_mainImageLayoutConstraint setConstant:-215.0];
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)sendPhoneNumber:(id)sender {
    [_titleTextLabel setText:@"أدخل الرمز الخاص بك"];
    [_stepsNumberLabel setText:@"خطوة ٢ من ٢"];
    [_inputValueTextField setPlaceholder:@"مثال : 45634"];
    [_condition1Label setHidden:YES];
    [_resendCodeButton setHidden:NO];
    [_changePhoneNumButton setHidden:NO];
    [_sendButton setTitle:@"أدخل التطبيق" forState:UIControlStateNormal];
    [_sendButton setEnabled:NO];
    [_condition2Label setText:@"بدخولك التطبيق أنت توافق على سياسة الخصوصية واتفاقية الاستخدام"];
    [_inputViewLayoutConstraint setConstant:0];
    [_mainImageLayoutConstraint setConstant:0];
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
    [_inputValueTextField resignFirstResponder];
}

- (IBAction)changePhoneNumber:(id)sender {
    [_titleTextLabel setText:@"تسجيل الدخول"];
    [_stepsNumberLabel setText:@"خطوة ١ من ٢"];
    [_inputValueTextField setPlaceholder:@""];
    [_condition1Label setHidden:NO];
    [_resendCodeButton setHidden:YES];
    [_changePhoneNumButton setHidden:YES];
    [_sendButton setTitle:@"إرسال" forState:UIControlStateNormal];
    [_sendButton setEnabled:YES];
    [_condition2Label setText:@"قم بتسجيل الدخول برقمك الخاص من شركة جوال لتحصل على محتوى خاص. يجب أن يكون عمرك فوق ١٨ حتى تستطيع استعمال الخدمة"];
}
@end

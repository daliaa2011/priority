//
//  SearchViewController.h
//  Priority
//
//  Created by AnAs EiD on 5/31/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "AppDataDelegate.h"
#import "SharedDataObject.h"

#import "OfferCustomTableViewCell.h"
#import "OfferDetailsViewController.h"

@interface SearchViewController : UIViewController<UITableViewDataSource,UITableViewDelegate> {
    
}

@property (weak, nonatomic) IBOutlet UISearchBar *offersSearchBar;
@property (weak, nonatomic) IBOutlet UITableView *searchedOffersTableView;
@property (weak, nonatomic) IBOutlet UIButton *layerButton;

@end

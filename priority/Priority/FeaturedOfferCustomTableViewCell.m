//
//  FeaturedOfferCustomTableViewCell.m
//  Priority
//
//  Created by AnAs EiD on 4/27/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "FeaturedOfferCustomTableViewCell.h"
#import "UIImageView+WebCache.h"

@implementation FeaturedOfferCustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.offerLogoImageView.alpha = 0.85;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

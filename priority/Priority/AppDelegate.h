//
//  AppDelegate.h
//  Priority
//
//  Created by AnAs EiD on 4/12/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "AppDataDelegate.h"
#import "SharedDataObject.h"
#import <GoogleMaps/GoogleMaps.h>

#import "PriorityDAO.h"
#import "SideMenuViewController.h"
#import "MainScreenViewController.h"
#import "MFSideMenuContainerViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SharedDataObject *theSharedDataObject;

//@property (strong, nonatomic) SideMenuViewController    *rightSideMenuView;
@property (strong, nonatomic) MainScreenViewController  *mainScreenView;
@property (strong, nonatomic) UINavigationController    *navigationController;

@end


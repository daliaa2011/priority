//
//  SideMenuViewController.m
//  Priority
//
//  Created by AnAs EiD on 4/13/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "SideMenuViewController.h"


@interface SideMenuViewController ()

@end

@implementation SideMenuViewController

- (SharedDataObject *)theAppDataObject{
    id<AppDataDelegate>delegate = (id<AppDataDelegate>)[UIApplication sharedApplication].delegate;
    SharedDataObject *dataObject;
    dataObject = (SharedDataObject *)delegate.theSharedDataObject;
    return dataObject;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        sharedDataObject = [self theAppDataObject];
        sideMenuModel = [[SideMenuModel alloc]init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.frame = [[UIScreen mainScreen] bounds];
    offersCategories = [[NSMutableArray alloc]initWithArray:[sideMenuModel getOfferCategories]];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    offersCategories = [[NSMutableArray alloc]initWithArray:[sideMenuModel getOfferCategories]];

    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"categoryCell";
    UITableViewCell *categoryCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UILabel     *offersCount;
    UILabel     *cellText;
    UIImageView *categoryIcon;
    
    if (categoryCell == nil) {
        categoryCell = [[UITableViewCell alloc]init];
        
        offersCount = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 5.0, 50.0, 30.0)];
        [offersCount setTextAlignment:NSTextAlignmentCenter];
        [offersCount setTextColor:[UIColor colorWithRed:34.0/255.0 green:179.0/255.0 blue:232.0/255.0 alpha:1]];
        [offersCount setFont:[UIFont fontWithName:@"Droid Arabic Kufi" size:14.0]];
        
        cellText = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 5.0, self.view.frame.size.width - 35.0, 30.0)];
        [cellText setTextAlignment:NSTextAlignmentRight];
        [cellText setTextColor:[UIColor whiteColor]];
        [cellText setFont:[UIFont fontWithName:@"Droid Arabic Kufi" size:14.0]];
        
        categoryIcon = [[UIImageView alloc]initWithFrame:CGRectMake(cellText.frame.size.width, 5.0, 30.0, 30.0)];
        
        [offersCount setTag:10];
        [cellText setTag:11];
        [categoryIcon setTag:12];
        
        [[categoryCell contentView]addSubview:offersCount];
        [[categoryCell contentView]addSubview:cellText];
        [[categoryCell contentView]addSubview:categoryIcon];
    }
    
    if (indexPath.section == 0) {
        
        BOOL englishLang = [[NSUserDefaults standardUserDefaults] boolForKey:@"englishLang"];
        if (englishLang){
            [cellText setText:[[offersCategories objectAtIndex:indexPath.row] NameEn]];
        }
        else {
            [cellText setText:[[offersCategories objectAtIndex:indexPath.row] name]];
        }
        
        [categoryIcon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%ld",(long)indexPath.row + 1]]];
        
    }
    else{
        [cellText setText:@"أنا مميز"];
    }
    [categoryCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //[offersCount setText:@"99"];
    
    return categoryCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return [offersCategories count];
    }
    else {
        return 1;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [cell setBackgroundColor:[UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:0]];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[self menuContainerViewController]toggleRightSideMenuCompletion:^{}];
    
    //category or custom sections 
    if (indexPath.section) {
        MyPriorityViewController *myPriorityView = [[MyPriorityViewController alloc]init];
        [[self navigationController]pushViewController:myPriorityView animated:YES];
    }
    else {
        CategoryOffersViewController *categoryOffersView = [[CategoryOffersViewController alloc]initWithNibName:@"CategoryOffersViewController" bundle:nil CategoryID:indexPath.row + 1];
        [[self navigationController]pushViewController:categoryOffersView animated:NO];
    }
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 10.0, tableView.frame.size.width - 5, 50.0)];
        [titleLabel setText:@"استكشف"];
        [titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:16.0]];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setTextAlignment:NSTextAlignmentRight];
        return titleLabel;
    }
    else {
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 10.0, tableView.frame.size.width - 5, 50.0)];
        [titleLabel setText:@"أقسامي"];
        [titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:16.0]];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setTextAlignment:NSTextAlignmentRight];
        return titleLabel;
    }
}
@end

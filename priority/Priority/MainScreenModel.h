//
//  MainScreenModel.h
//  Priority
//
//  Created by AnAs EiD on 5/10/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "AppDataDelegate.h"
#import "SharedDataObject.h"

@interface MainScreenModel : NSObject

- (NSString *)getCategoryNameForID:(int)categoryID;
@end

//
//  OfferTypeList.h
//  Priority
//
//  Created by AnAs EiD on 4/20/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfferTypeList : NSObject

@property int _ID;
@property (retain, nonatomic) NSString *name;
@property int typeID;
@property (retain, nonatomic) NSString *details;
@property BOOL isActive;

@end

//
//  OfferTBL.h
//  Priority
//
//  Created by AnAs EiD on 4/20/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfferTBL : NSObject

@property int _ID;
@property (retain, nonatomic) NSString *name;
@property int bussinessTBLID;
@property (retain, nonatomic) NSString *logo;
@property int catID;
@property int offerTypesTBLID;
@property int quantity;
@property (retain, nonatomic) NSDate *startDate;
@property (retain, nonatomic) NSDate *endDate;
@property (retain, nonatomic) NSString *phoneNum;
@property (retain, nonatomic) NSString *details;
@property int offerNum;
@property int getFree;
@property (retain, nonatomic) NSString *restrictKeys;
@property (retain, nonatomic) NSString *restrictType;
@property BOOL sat;
@property BOOL sun;
@property BOOL mon;
@property BOOL tue;
@property BOOL wed;
@property BOOL thr;
@property BOOL fri;
@property (retain, nonatomic) NSDate *startTime;
@property (retain, nonatomic) NSDate *endTime;
@property BOOL cond1;
@property BOOL cond2;
@property BOOL cond3;
@property BOOL cond4;
@property BOOL cond5;
@property BOOL cond6;
@property BOOL cond7;
@property BOOL extraCond1;
@property BOOL extraCond2;
@property BOOL extraCond3;
@property (retain, nonatomic) NSString *vCode;
@property BOOL isStaticV;
@property int codeType;
@property (retain, nonatomic) NSString *codeQuote;
@property (retain, nonatomic) NSDate *insertDate;
@property (retain, nonatomic) NSDate *updateDate;
@property BOOL isActive;
@property (retain, nonatomic) NSString *title;
@property (retain, nonatomic) NSString *OfferDesc;


@end

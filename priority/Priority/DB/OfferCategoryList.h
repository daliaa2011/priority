//
//  OfferCategoryList.h
//  Priority
//
//  Created by AnAs EiD on 4/20/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfferCategoryList : NSObject

@property int _ID;
@property (retain, nonatomic) NSString *name;
@property (retain, nonatomic) NSString *NameEn;
@property (retain, nonatomic) NSString *iconName;
@property int typeID;
@property (retain, nonatomic) NSString *details;
@property BOOL isActive;

@end

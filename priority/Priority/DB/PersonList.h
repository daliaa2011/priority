//
//  PersonList.h
//  Priority
//
//  Created by AnAs EiD on 4/20/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonList : NSObject

@property int _ID;
@property (retain, nonatomic) NSString *name;
@property (retain, nonatomic) NSString *userName;
@property (retain, nonatomic) NSDate *birthDate;
@property (retain, nonatomic) NSString *email;
@property (retain, nonatomic) NSString *phoneNumber;
@property BOOL isMale;
@property (retain, nonatomic) NSDate *RegistrationDate;
@end

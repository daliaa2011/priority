//
//  PriorityDAO.m
//  Priority
//
//  Created by AnAs EiD on 4/15/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import "PriorityDAO.h"

#import <MapKit/MapKit.h>

@implementation PriorityDAO

+ (BOOL)createEditableCopyOfDatabaseIfNeeded{
    // First, test for existence.
    BOOL success;
    BOOL newInstallation;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"PriorityDB.sqlite"];
    
    success = [fileManager fileExistsAtPath:writableDBPath];
    newInstallation = !success;
    if (success) {
        return newInstallation;
    }
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:@"PriorityDB.sqlite"];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
    return newInstallation;
}

- (BOOL)connectDB{
    BOOL result;
    @try {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"PriorityDB.sqlite"];
        BOOL success = [fileManager fileExistsAtPath:writableDBPath];
        if (!success) {
            result = success;
        }
        if (!(sqlite3_open([writableDBPath UTF8String], &priorityDB) == SQLITE_OK)) {
            result = NO;
        }
    }
    @catch (NSException *exception) {
        result = NO;
    }
    @finally {
        return result;
    }
}

- (BOOL)disConnectDB{
    int result = sqlite3_close(priorityDB);
    return result;
}

- (BOOL)insertData:(NSString *)query{
    BOOL result = NO;
    char *error;
    const char *sql = [query UTF8String];
    if (sqlite3_exec(priorityDB, sql, NULL, NULL, &error) == SQLITE_OK) {
        result = YES;
    }
    else{
        NSLog(@"%s",sqlite3_errmsg(priorityDB));
    }
    return result;
}

- (NSMutableArray *)getOfferCategories:(const char *)query{
    NSMutableArray *offerCategories = [[NSMutableArray alloc]init];
    const char *sql = query;
    sqlite3_stmt *sqlStatment;
    
    @try {
        if (sqlite3_prepare(priorityDB, sql, -1, &sqlStatment, NULL) == SQLITE_OK) {
            while (sqlite3_step(sqlStatment) == SQLITE_ROW) {
                OfferCategoryList *offerCategory = [[OfferCategoryList alloc]init];
                offerCategory._ID = sqlite3_column_int(sqlStatment, 0);
                offerCategory.name = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(sqlStatment, 1)];
                offerCategory.typeID = sqlite3_column_int(sqlStatment, 2);
                offerCategory.details = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(sqlStatment, 3)];
                offerCategory.isActive = sqlite3_column_int(sqlStatment, 4);
                
                offerCategory.iconName = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(sqlStatment, 5)];
                
                offerCategory.NameEn = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(sqlStatment, 6)];
                [offerCategories addObject:offerCategory];
            }
        }
        //Prepare Statment problem
        sqlite3_finalize(sqlStatment);
    }
    @catch (NSException *exception) {
        NSLog(@"Exception : %@",exception);
    }
    @finally {
        return offerCategories;
    }
}

- (NSMutableArray *)getOffers:(const char *)query{
    NSMutableArray *offers = [[NSMutableArray alloc]init];
    const char *sql = query;
    sqlite3_stmt *sqlStatment;
    
    @try {
        if (sqlite3_prepare(priorityDB, sql, -1, &sqlStatment, NULL) == SQLITE_OK) {
            while (sqlite3_step(sqlStatment) == SQLITE_ROW) {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
                //[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 2)]
                //[NSNumber numberWithInt:sqlite3_column_int(sqlStatment, 0)]
                NSMutableDictionary *offerObject = [NSMutableDictionary dictionary];
                [offerObject setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 0)]
                               forKey:@"businessAddress"];
                
                [offerObject setValue:[NSNumber numberWithInt:sqlite3_column_int(sqlStatment, 1)]
                               forKey:@"CatsTBLID"];
                
                [offerObject setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 2)]
                               forKey:@"businessCity"];
                
                [offerObject setValue:[NSNumber numberWithInt:sqlite3_column_int(sqlStatment, 3)]
                               forKey:@"CountryId"];
                
                [offerObject setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 4)]
                               forKey:@"businessDetails"];
                
                [offerObject setValue:[NSNumber numberWithInt:0]
                               forKey:@"businessDistance"];
                
                [offerObject setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 5)]
                               forKey:@"businessID"];
                
                [offerObject setValue:[[CLLocation alloc] initWithLatitude:[[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 6)] doubleValue] longitude:[[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 8)] doubleValue]]
                               forKey:@"businessLocation"];
                
                [offerObject setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 7)]
                               forKey:@"businessLogo"];
                
                [offerObject setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 9)]
                               forKey:@"businessName"];
                
                [offerObject setValue:[NSNumber numberWithInt:sqlite3_column_int(sqlStatment, 10)]
                               forKey:@"offerCodeType"];
                
                [offerObject setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 11)]
                               forKey:@"offerDetails"];
                
                [offerObject setValue:[dateFormatter dateFromString:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 12)]]
                               forKey:@"offerEndDate"];
                
                [offerObject setValue:[dateFormatter dateFromString:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 13)]]
                               forKey:@"offerEndTime"];
                
                [offerObject setValue:[NSNumber numberWithInt:sqlite3_column_int(sqlStatment, 14)]
                               forKey:@"offerID"];
                
                [offerObject setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 15)]
                               forKey:@"offerLogo"];
                
                [offerObject setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 16)]
                               forKey:@"offerName"];
                
                [offerObject setValue:[dateFormatter dateFromString:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 17)]]
                               forKey:@"offerStartDate"];
                
                [offerObject setValue:[dateFormatter dateFromString:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 18)]]
                               forKey:@"offerStartTime"];
                
                [offerObject setValue:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 19)]
                               forKey:@"offerVCode"];
                
                [offers addObject:offerObject];
            }
        }
        //Prepare Statment problem
        sqlite3_finalize(sqlStatment);
    }
    @catch (NSException *exception) {
        NSLog(@"Exception : %@",exception);
    }
    @finally {
        return offers;
    }
}

- (int)getIntegerValue:(const char *)query{
    int integer = 0;
    const char *sql = query;
    sqlite3_stmt *sqlStatment;
    @try {
        if (sqlite3_prepare(priorityDB, sql, -1, &sqlStatment, NULL) == SQLITE_OK) {
            while (sqlite3_step(sqlStatment) == SQLITE_ROW) {
                integer = sqlite3_column_int(sqlStatment, 0);
            }
            sqlite3_finalize(sqlStatment);
        }
        //Prepare Statment problem
    }
    @catch (NSException *exception) {
        //Can be replaced with exception Log
    }
    @finally {
        return integer;
    }
}

- (NSString *)getDataText:(const char *)query{
    NSString *text = [[NSString alloc]initWithUTF8String:"new"];
    const char *sql = query;
    sqlite3_stmt *sqlStatment;
    @try {
        if (sqlite3_prepare(priorityDB, sql, -1, &sqlStatment, NULL) == SQLITE_OK) {
            while (sqlite3_step(sqlStatment) == SQLITE_ROW) {
                text = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatment, 0)];
            }
            sqlite3_finalize(sqlStatment);
        }
    }
    @catch (NSException *exception) {
        //Can be replaced with exception Log
        NSLog(@"Exception : %@",exception);
    }
    @finally {
        return text;
    }
}
@end

//
//  PriorityDAO.h
//  Priority
//
//  Created by AnAs EiD on 4/15/15.
//  Copyright (c) 2015 Arab Mobile Content. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

#import "OfferCategoryList.h"

@interface PriorityDAO : NSObject{
    sqlite3 *priorityDB;
    
}

+ (BOOL)createEditableCopyOfDatabaseIfNeeded;

- (BOOL)connectDB;
- (BOOL)disConnectDB;

- (BOOL)insertData:(NSString *)query;

- (NSMutableArray *)getOfferCategories:(const char *)query;
- (NSMutableArray *)getOffers:(const char *)query;

- (NSString *)getDataText:(const char *)query;
- (int)getIntegerValue:(const char *)query;
@end
